sap.ui.define(
    [
        "sap/ui/core/mvc/Controller",
        "sap/ui/core/UIComponent",
        "sap/m/library",
        "../model/formatter",
        "sap/m/MessagePopover",
        "sap/m/MessageItem",
        "sap/ui/core/Fragment",
    ],
    function (
        Controller,
        UIComponent,
        mobileLibrary,
        formatter,
        MessagePopover,
        MessageItem,
        Fragment
    ) {
        "use strict";

        // shortcut for sap.m.URLHelper
        var URLHelper = mobileLibrary.URLHelper;

        var oMessageTemplate = new MessageItem({
            type: '{type}',
            title: '{message}',
            activeTitle: "{active}",
            description: '{description}',
            subtitle: '{subtitle}',
            counter: '{counter}',
            markupDescription: true
        });

        var oMessagePopover = new MessagePopover({
            items: {
                path: "/",
                template: oMessageTemplate
            }
            // groupItems: true
        });
        oMessagePopover.setModel(sap.ui.getCore().getMessageManager().getMessageModel());

        return Controller.extend(
            "be.amista.createpo.controller.BaseController", {
                formatter: formatter,

                onNavBack: function () {
                    var sPreviousHash = History.getInstance().getPreviousHash();
                    if (sPreviousHash !== undefined) {
                        history.go(-1);
                    } else {
                        //this.getRouter().navTo("master", {}, true);
                        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                        oRouter.navTo("master", {}, true);
                    }
                },

                /**
                 * Convenience method for accessing the router.
                 * @public
                 * @returns {sap.ui.core.routing.Router} the router for this component
                 */
                getRouter: function () {
                    return UIComponent.getRouterFor(this);
                },
                getGlobalModel: function (sName) {
                    return this.getOwnerComponent().getModel(sName);
                },
                getModel: function (sName) {
                    return this.getView().getModel(sName);
                },
                /**
                 * Convenience method for setting the view model.
                 * @public
                 * @param {sap.ui.model.Model} oModel the model instance
                 * @param {string} sName the model name
                 * @returns {sap.ui.mvc.View} the view instance
                 */
                setModel: function (oModel, sName) {
                    return this.getView().setModel(oModel, sName);
                },

                /**
                 * Getter for the resource bundle.
                 * @public
                 * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
                 */
                getResourceBundle: function () {
                    return this.getOwnerComponent().getModel("i18n").getResourceBundle();
                },

                /**
                 * Event handler when the share by E-Mail button has been clicked
                 * @public
                 */
                onShareEmailPress: function () {
                    var oViewModel =
                        this.getModel("objectView") || this.getModel("worklistView");
                    URLHelper.triggerEmail(
                        null,
                        oViewModel.getProperty("/shareSendEmailSubject"),
                        oViewModel.getProperty("/shareSendEmailMessage")
                    );
                },
                /**
                 * This method requires data and a name to create a JSON model and bind it to the view
                 * @param {data}
                 * @param {name}
                 */
                createModelAndSetToView: function (data, name) {
                    var oModel = new sap.ui.model.json.JSONModel(data);
                    this.getView().setModel(oModel, name);
                },
                /**
                 * Displays a sap.m.MessageBox error with a given message and an error
                 * @param sMessage
                 * @param oError
                 */
                showErrorMessage: function (oError, sMessage) {
                    sap.ui.core.BusyIndicator.hide();
                    // this.getOwnerComponent().getModel("local").setProperty("/busy", false);
                    sap.m.MessageBox.error(
                        sMessage ?
                        sMessage + this.getResourceBundle().getText("errorTextExtended") :
                        this.getResourceBundle().getText("errorText"), {
                            title: this.getResourceBundle().getText(
                                "generalErrorMessageTitle"
                            ),
                            details: formatter.compressErrorObject(oError),
                        }
                    );
                },
                /**
                 * Fetches the environment where the app is opened
                 * @returns account Object
                 */
                getAccountName: function () {
                    var account =
                        document.URL.split(".").filter(function (element) {
                            if (element.indexOf("danonedeveu") !== -1) return true;
                            else return false;
                        }).length > 0 ?
                        "danonedeveu" :
                        document.URL.split(".").filter(function (element) {
                            if (element.indexOf("danonequaeu") !== -1) return true;
                            else return false;
                        }).length > 0 ?
                        "danonequaeu" :
                        "danoneprodeu";
                    return account;
                },
                /**
                 * When a user presses an item (or a button) in the list/table, this method returns the object that is bound to that row
                 * @param oEvent = the press event of an item in the list/table
                 * @returns object from a model attached to the Component
                 */
                getObjectFromModelAfterSelectedInTableWithoDataBinding: function (oEvent, sNameOfModel) {
                    var sPathForObject = oEvent.getSource().getBindingContext().getPath();
                    return this.getPropertyFromModelViaPath(sPathForObject, sNameOfModel);
                },
                getPropertyFromModelViaPath: function (sPath, sNameOfModel) {
                    if (!sNameOfModel)
                        // We want the default model
                        return this.getOwnerComponent().getModel().getProperty(sPath);
                    else
                        return this.getOwnerComponent()
                            .getModel(sNameOfModel)
                            .getProperty(sPath);
                },
                setObjectFromDefaultModelAfterSelectedInTableWithoDataBinding: function (oEvent, oObject) {
                    var sPathForObject = oEvent.getSource().getBindingContext().getPath();
                    return this.setPropertyFromDefaultModelViaPath(
                        sPathForObject,
                        oObject
                    );
                },
                setPropertyFromDefaultModelViaPath: function (sPath, oObject) {
                    return this.getOwnerComponent()
                        .getModel()
                        .setProperty(sPath, oObject);
                },

                getTextOfColumnByColumnHeaderBindingPathToi18nModelInTable: function (oEvent, sTableId, sColumnNamePathi18n) {
                    var iIndex = this.getIndexOfColumnHeaderBindingPathToi18nModelInTable(
                        sTableId,
                        sColumnNamePathi18n
                    );
                    return oEvent.getSource().getParent().getCells()[iIndex].getAggregation("items").pop().getText();
                },
                getIndexOfColumnHeaderBindingPathToi18nModelInTable: function (sTableId, sColumnNamePathi18n) {
                    var iIndex = 0;
                    var aColumns = this.byId(sTableId).getColumns();

                    for (var i = 0; i < aColumns.length; i++) {
                        if (aColumns[i].getHeader() === null) continue; // continue jumps out of the loop
                        if (
                            aColumns[i].getHeader().getBinding("text").getPath() ===
                            sColumnNamePathi18n
                        ) {
                            iIndex = i;
                            break;
                        }
                    }
                    return iIndex;
                },
                setResourceBundle: function (sLanguage) {
                    var i18nModel = new sap.ui.model.resource.ResourceModel({
                        bundleName: "com.agre.PM_Notification.i18n.i18n",
                        bundleUrl: "i18n/i18n.properties",
                        bundleLocale: sLanguage,
                        fallbackLocale: sLanguage,
                        supportedLocales: [sLanguage],
                    });

                    this.getOwnerComponent().setModel(i18nModel, "i18n");
                    sap.ui.getCore().getConfiguration().setLanguage(sLanguage.toUpperCase());
                },
                setDateFormatForLanguage: function (sLanguage) {
                    this.getModel("worklistView").setProperty("/UserLanguage", sLanguage);
                    var sDateFormat = formatter.formatDateFormatDependingOnLanguageSettings(
                        sLanguage
                    );
                    this.getModel("worklistView").setProperty("/DateFormat", sDateFormat);
                    this.getModel("worklistView").setProperty(
                        "/MomentDateFormat",
                        sDateFormat.toUpperCase()
                    );
                },
                getCurrentUserLanguage: function () {
                    var sLanguage = sap.ui.getCore().getConfiguration().getLocale().getLanguage();
                    if (sLanguage) {
                        this.setResourceBundle(sLanguage);
                        this.setDateFormatForLanguage(sLanguage);
                    }
                },
                getHeaderSetPath: function (sGuid) {
                    return "/HeaderSet" + "(" + "guid" + "'"  + sGuid + "'" + ")";
                },
                getGuid: function (bNew) {
                    var that = this;
                    return new Promise(function (resolve, reject) {
                        var sGuid = that.getVar("Guid");
                        if((!sGuid)||(bNew)) sGuid = "0";
                        if (sGuid === "0") {
                            // If there is no GUID yet, request one from the backend
                            var sPath = "/DraftGuidSet('0')";
                            that.getOwnerComponent().getModel().read(sPath, {
                                success: function (oResponse) {
                                    that.storeVar("Guid", oResponse.Guid);
                                    resolve(oResponse.Guid);
                                },
                                error: function (oError) {
                                    reject(oError);
                                },
                            });
                        } else {
                            resolve(sGuid); // There is already a Guid
                        }
                    });
                },
                initGuid: function (oView, bExpandItemsAndAttachments) {
                    var that = this;
                    return new Promise(function (resolve, reject) {
                        // var sOldGuid = that.getVar("Guid");
                        that.getGuid().then(function (sGuid) {
                            // Load or refresh binding on Wizard View level
                            //var sPath = "/" + that.getOwnerComponent().getModel().createKey("HeaderSet", {
                            //    Guid: sGuid,
                            //});
                            var sPath = that.getHeaderSetPath(sGuid);
                            // if (sOldGuid === sGuid) {
                            // 	var oBinding = oView.getElementBinding();
                            // }
                            if (!oBinding) {
                                if (bExpandItemsAndAttachments) {
                                    oView.bindElement({
                                        path: sPath,
                                        parameters: {
                                            expand: "to_Vendor,to_Items"
                                        }
                                    });
                                } else {
                                    oView.bindElement({
                                        path: sPath,
                                        parameters: {
                                            expand: "to_Vendor,to_Items"
                                        }
                                    });
                                    if (Fragment.byId(that.getView().createId("lineItems"), "itemTable")) {
                                        Fragment.byId(that.getView().createId("lineItems"), "itemTable").bindElement({
                                            path: sPath + "/to_Items"
                                        });
                                    }
                                }
                                var oBinding = oView.getElementBinding();
                                // Always resolve using datareceived event, once
                                oBinding.attachEventOnce("dataReceived", function (oData) {
                                    var oHeader = oData.getParameter("data");
                                    resolve(oHeader);
                                });
                            } else {
                                // oBinding.refresh(true);
                                resolve(that.getOwnerComponent().getModel().getProperty(sPath));
                            }
                        }).catch(function (oError) {
                            reject(oError);
                        });
                    });
                },
                storeVar: function (sName, value) {
                    this.getOwnerComponent().getModel("local").setProperty("/" + sName, value);
                },
                getVar: function (sName) {
                    try {
                        return this.getOwnerComponent().getModel("local").getProperty("/" + sName);
                    } catch (oError) {
                        return null;
                    }
                },
                getMessageManager: function () {
                    return sap.ui.getCore().getMessageManager();
                },
                getMessageModel: function () {
                    return sap.ui.getCore().getMessageManager().getMessageModel();
                },
                onMessagePress: function (oEvent) {
                    var oControl;
                    if (oEvent.srcControl) {
                        oControl = oEvent.srcControl;
                    } else {
                        oControl = oEvent.getSource();
                    }
                    if (oControl) oMessagePopover.openBy(oControl);
                },
                validateEmail: function (sEmail) {
                    var ret = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    return ret.test(String(sEmail).toLowerCase());
                },
                /**
                 * Helper function to find an array object by property value
                 */
                findObjectByKey: function (array, key, value) {
                    for (var i = 0; i < array.length; i++) {
                        if (array[i][key] === value) {
                            return array[i];
                        }
                    }
                    return null;
                },
                addDoubleClickEventToTreeTable: function (oTable) {
                    // Add logic for the double click event on a node in the tree table
                    var that = this;
                    var iLastSelIndex = -1;

                    oTable.attachEvent("rowSelectionChange", function () {
                        if (oTable.getSelectedIndex() > -1) {
                            iLastSelIndex = oTable.getSelectedIndex();
                        }
                    });

                    oTable.attachBrowserEvent("dblclick", function () {
                        if (oTable.isIndexSelected(iLastSelIndex)) {
                            that.onHierarchySelect();
                        } else {
                            oTable.addSelectionInterval(iLastSelIndex, iLastSelIndex);
                        }
                        iLastSelIndex = -1;
                    });
                },
                makeNewValueWithOneDecimalAndNegative: function (value) {
                    var sNewValue = "";
                    for (var i = 0; i < value.length; i++) {
                        var char = value[i];
                        if (
                            (!isNaN(parseInt(char, 0)) ||
                                char === "-" ||
                                char === "." ||
                                char === ",") &&
                            sNewValue.charAt(sNewValue.length - 2) !== "."
                        )
                            sNewValue += char;
                    }
                    return sNewValue;
                },
                /**
                 * Makes a new value with decimals and possibility to be negative
                 * @param value
                 * @returns String
                 */
                makeNewValueWithDecimalsAndNegative: function (sValue) {
                    var sNewValue = "";
                    for (var i = 0; i < sValue.length; i++) {
                        var char = sValue[i];
                        if (
                            !isNaN(parseInt(char, 0)) ||
                            char === "-" ||
                            char === "." ||
                            char === ","
                        )
                            sNewValue += char;
                    }
                    return sNewValue;
                },
                getPropertyMaxLength: function (sEntity, sProperty) {
                    var oServiceMetadata = this.getOwnerComponent().getModel().getServiceMetadata();
                    var oHeaderMetadata = oServiceMetadata.dataServices.schema[0].entityType.filter(oEntity => oEntity.name === sEntity)[0];
                    var oFieldMetadata = oHeaderMetadata.property.filter(oProp => oProp.name === sProperty)[0];
                    return parseInt(oFieldMetadata.maxLength);
                },
            }
        );
    }
);