sap.ui.define(
    [
        "./BaseController",
        // "sap/ui/model/json/JSONModel",
        // "sap/m/MessageToast",
        "sap/m/MessageBox",
        // "sap/ui/model/Filter",
        // "sap/ui/model/FilterOperator",
        "sap/ui/core/Fragment",
        "be/amista/createpo/util/AjaxHelper",
        "be/amista/createpo/util/AttachmentHelper",
        "be/amista/createpo/util/ObjectHelper",
        "be/amista/createpo/util/ValueListHelper",
        "../model/formatter",
        // 'sap/m/SearchField',
        // 'sap/m/ColumnListItem',
        // 'sap/m/Token',
        // 'sap/ui/model/type/String',
        // 'sap/ui/comp/library',
        "sap/ui/model/resource/ResourceModel",
        // "sap/m/Popover",
        // "sap/m/FormattedText"
    ],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (
        BaseController,
        // JSONModel,
        // MessageToast,
        MessageBox,
        // Filter,
        // FilterOperator,
        Fragment,
        AjaxHelper,
        AttachmentHelper,
        ObjectHelper,
        ValueListHelper,
        formatter,
        // SearchField,
        // ColumnListItem,
        // Token,
        // TypeString,
        // compLibrary,
        ResourceModel,
        // Popover,
        // FormattedText
    ) {
        "use strict";
        /*global moment */
        return BaseController.extend("be.amista.createpo.controller.main", {
            formatter: formatter,
            onInit: function () {
                var that = this;
                this.getOwnerComponent().getModel().metadataLoaded().then(function () {
                    that.getOwnerComponent().getComponentData().changingSourcePageAllowed = true;
                    that.getRouter().getRoute("Create").attachPatternMatched(that._onObjectMatched, that);
                    that.getRouter().getRoute("Edit").attachPatternMatched(that._onObjectMatched, that);
                    // busy indicator during batch requests
                    // that.getOwnerComponent().getModel().attachBatchRequestSent(that._onBatchRequestStarted, that);
                    // that.getOwnerComponent().getModel().attachBatchRequestCompleted(that._onBatchRequestFinished, that);
                    // that.getOwnerComponent().getModel().attachBatchRequestFailed(that._onBatchRequestFinished, that);
                });
                // used in ValueListHelper to create dialogs
                this.fragmentId = "frcurrencies";
                this.fragmentIdCC = "frcompanyCodes";
                this.fragmentIdV = "frvendors";
                this.fragmentIdPOHistory = "frpoHistory";
                this.fragmentIdPOHistoryAttachments = "frpoHistoryAttachments";
                this.fragmentIdWbs = "frwbs";
                this.fragmentIdTaxCode = "frtaxCodes";
                this.fragmentIdUoM = "fruom";
                this.fragmentIdMG = "frmaterialGroups";
                this.fragmentIdIO = "frinternalOrders";
                this.fragmentIdCostCenter = "frcostCenters";
                this.fragmentIdPurGrp = "frpurchaseGroups";
                this.fragmentIdAcct = "fracctAssignment";
                this.fragmentIdAtt = "frattachments";
                this.fragmentIdItems = "frlineItems";
                this.fragmentIdPreview = "frpreview";
                this.fragmentIdFieldHelp = "frfieldHelp";
                this.oModel = this.getOwnerComponent().getModel();
                this.oModel.setDefaultBindingMode("TwoWay");
                this.getView().setModel(new sap.ui.model.json.JSONModel(ObjectHelper.getMasterViewData()), "masterView");
                this.getView().setModel(this.getOwnerComponent().getModel("local"), "local");
                AttachmentHelper.initializeAttachments(this);
                ValueListHelper.initializeValueLists(this);
                // set i18n model on view
                var i18nModel = new ResourceModel({
                    bundleName: "be.amista.createpo.i18n.i18n"
                });
                this.getView().setModel(i18nModel, "i18n");
                // register manager for message handling
                this.getMessageManager().registerObject(this.getView(), true);
                // open messages directly when received
                this.getView().byId("messagesIndicator").addEventDelegate({
                    onAfterRendering: function (oEvent) {
                        that.onMessagePress(oEvent);
                    }
                });
            },
            /* =========================================================== */
            /* Value helps          				                       */
            /* =========================================================== */
            onSearchProject: function (oEvent) {
                var sSearch = oEvent.getSource().getValue();
                var oProjectList = Fragment.byId(this.fragmentIdWbs, "projectList");
                var oBindingInfo = oProjectList.getBindingInfo("items");
                if (!oBindingInfo) return;
                if (!oBindingInfo.parameters) oBindingInfo.parameters = {};
                if (!oBindingInfo.parameters.custom) oBindingInfo.parameters.custom = {};
                oBindingInfo.parameters.custom.search = sSearch;
                oProjectList.bindItems(oBindingInfo);
            },
            onProjectPress: function (oEvent) {
                var oProject = oEvent.getSource().getBindingContext().getObject();
                var oWBSTable = Fragment.byId(this.fragmentIdWbs, "wbsTable");
                var oBindingInfo = oWBSTable.getBindingInfo("items");
                if (!oBindingInfo) return;
                var aFilters = [];
                aFilters.push(new sap.ui.model.Filter("Pspid", sap.ui.model.FilterOperator.EQ, oProject.Pspid));
                oBindingInfo.filters = aFilters;
                oWBSTable.bindAggregation("items", oBindingInfo);
            },
            onSearchWBS: function (oEvent) {
                var sSearch = oEvent.getSource().getValue();
                var oWBSTable = Fragment.byId(this.fragmentIdWbs, "wbsTable");
                var oBindingInfo = oWBSTable.getBindingInfo("items");
                if (!oBindingInfo) return;
                if (!oBindingInfo.parameters) oBindingInfo.parameters = {};
                if (!oBindingInfo.parameters.custom) oBindingInfo.parameters.custom = {};
                oBindingInfo.parameters.custom.search = sSearch;
                oWBSTable.bindAggregation("items", oBindingInfo);
            },
            onCloseWBS: function (oEvent) {
                this._oDialogWBS.close();
            },
            onWBSSelect: function (oEvent) {
                var oWBS = oEvent.getParameter("listItem").getBindingContext().getObject()
                var oForm = Fragment.byId(this.getView().createId("lineItems"), "itemForm");
                var sPath = oForm.getBindingContext().getPath();
                this.getOwnerComponent().getModel().setProperty(sPath + "/Posid", oWBS.Posid);
                this.getView().getModel("masterView").refresh();
                AjaxHelper.saveChanges(this.getOwnerComponent().getModel());
                this.onCloseWBS();
            },
            handleSearchInValueList: function (oEvent) {
                var sSearchValue = oEvent.getParameter("value");
                var oListItemTemplate;
                if (oEvent.getSource().getBindingInfo("items")) {
                    oListItemTemplate = oEvent.getSource().getBindingInfo("items").template;
                } else {
                    oListItemTemplate = oEvent.getSource().getItems()[0].clone();
                }
                oEvent.getSource().bindItems({
                    path: oEvent.getSource().getBinding("items").getPath(),
                    parameters: {
                        // expand: "to_Children",
                        custom: {
                            // The search paramater is a custom url parameter (no Odata $search).
                            // The BE filters the Value Lists on several properties in that Value List with this custom url parameter.
                            search: sSearchValue === null ? "" : sSearchValue, // An example how it looks: 'VL_HierarchySet('RX143')/to_Children?$filter=(AssetInd%20eq%20false%20and%20LocationInd%20eq%20true)&search=Dak&$skip=0&$top=110&$inlinecount=allpages'
                        },
                    },
                    template: oListItemTemplate,
                });
            },
            onTrcFieldHelpRequest: function (oEvent) {
                this.onFieldHelpRequest("TRC_HELP");
            },
            onAccountAssignmentFieldHelpRequest: function (oEvent) {
                this.onFieldHelpRequest("ACCOUNT_ASSIGNMENT_HELP");
            },
            onMaterialGroupFieldHelpRequest: function (oEvent) {
                this.onFieldHelpRequest("MATERIAL_GROUP_HELP");
            },
            onTaxCodeFieldHelpRequest: function (oEvent) {
                this.onFieldHelpRequest("TAX_CODE_HELP");
            },

            /* =========================================================== */
            /* Change events Header          				               */
            /* =========================================================== */
            /**
             * Called when the user changes a field of the header
             */
            onChangeHeader: function (sProperty, sValue) {
                var that = this;
                var oModel = this.getOwnerComponent().getModel();
                var sPath = this.getView().getElementBinding().getPath();
                oModel.setProperty(sPath + "/" + sProperty, sValue);
                return new Promise(function (resolve, reject) {
                    if (oModel.hasPendingChanges()) {
                        oModel.setRefreshAfterChange(true);
                        oModel.submitChanges({
                            success: function (oData, response) {
                                if (that.getView().getElementBinding()) that.getView().getElementBinding().refresh();
                                resolve(JSON.parse(oData.__batchResponses[1].body).d);
                            },
                            error: function (oError) {
                                that.showErrorMessage(oError, "something went wrong while trying to update an item");
                                reject(oError);
                            },
                        });
                    } else {
                        resolve(null);
                    }
                });
            },
            onChangeVendor: function (oEvent) {
                var that = this;
                var sVendorNr = oEvent.getParameter("value");
                this.onChangeHeader("VendorNr", sVendorNr).then(function (oData) {
                    that.setVendorInputValue(oData);
                });
            },
            onChangeEkgrp: function (oEvent) {
                var that = this;
                var sEkgrp = oEvent.getParameter("value");
                this.onChangeHeader("Ekgrp", sEkgrp).then(function (oData) {
                    that.setEkgrpInputValue(oData.Ekgrp);
                });
            },
            onChangeBukrs: function (oEvent) {
                var that = this;
                var sBukrs = oEvent.getParameter("value");
                this.onChangeHeader("Bukrs", sBukrs).then(function (oData) {
                    that.setBukrsInputValue(oData.Bukrs);
                    // indicators for VL refresh to filter on Bukrs
                    that.getOwnerComponent().getModel("local").setProperty("/refreshWBS", true);
                    that.getOwnerComponent().getModel("local").setProperty("/refreshCostCenter", true);
                    that.getOwnerComponent().getModel("local").setProperty("/refreshAsset", true);
                    that.getOwnerComponent().getModel("local").setProperty("/refreshInternalOrder", true);
                    that.getOwnerComponent().getModel("local").setProperty("/refreshTaxCode", true);
                });
            },
            setVendorInputValue: function (oData) {
                var that = this;
                var oVendorInput = Fragment.byId(this.getView().createId("headerInfo"), "vendorInput");
                if (oData && oData !== undefined && oData.to_Vendor.VendorNr !== "") {
                    this.setInputValue(oVendorInput, oData.to_Vendor.VendorNr, oData.to_Vendor.Name);
                } else {
                    var sPath = this.getView().getElementBinding().getPath();
                    var sVendorNr = this.getOwnerComponent().getModel().getProperty(sPath + "/VendorNr");
                    if (sVendorNr && sVendorNr !== "") {
                        this.getValueListObject("/VL_VendorSet", sVendorNr).then(function (oData) {
                            that.setInputValue(oVendorInput, oData.VendorNr, oData.Name);
                        }).catch(function (oError) {
                            that.setInputValue(oVendorInput);
                        });
                    } else {
                        that.setInputValue(oVendorInput, "", "");
                    }
                }
            },
            setEkgrpInputValue: function (sPurchasingGroup) {
                var that = this;
                var oEkgrpInput = Fragment.byId(this.getView().createId("headerInfo"), "ekgrpInput");
                if (sPurchasingGroup && sPurchasingGroup !== undefined && sPurchasingGroup !== "") {
                    this.getValueListObject("/C_PurchasingGroupValueHelp", sPurchasingGroup).then(function (oData) {
                        that.setInputValue(oEkgrpInput, oData.PurchasingGroup, oData.PurchasingGroupName);
                    }).catch(function (oError) {
                        that.setInputValue(oEkgrpInput);
                    });
                    this.setInputValue(oEkgrpInput, "", "");
                } else {
                    this.setInputValue(oEkgrpInput, "", "");
                }
            },
            setBukrsInputValue: function (sCompanyCode) {
                var that = this;
                var oBukrsInput = Fragment.byId(this.getView().createId("headerInfo"), "bukrsInput");
                if (sCompanyCode && sCompanyCode !== undefined && sCompanyCode !== "") {
                    this.getValueListObject("/C_MM_CompanyCodeValueHelp", sCompanyCode).then(function (oData) {
                        that.setInputValue(oBukrsInput, oData.CompanyCode, oData.CompanyCodeName);
                    }).catch(function (oError) {
                        that.setInputValue(oBukrsInput);
                    });
                } else {
                    this.setInputValue(oBukrsInput, "", "");
                }
            },
            onChangeStartAndEndDate: function (oEvent) {
                var sPath = this.getView().getElementBinding().getPath();
                this.changeDate(sPath, oEvent);
                this.saveChanges();
            },
            selectionTrcChanged: function (oEvent) {
                this.getOwnerComponent().getModel().setProperty(this.getView().getElementBinding().getPath() + "/Trc",
                    oEvent.getSource().getSelectedKey() === "true" ? true : false);
                this.saveChanges(oEvent);
            },
            /* =========================================================== */
            /* Change events Item             				               */
            /* =========================================================== */
            /**
             * Set binding to selected item
             */
            onSelectionChange: function (oEvent) {
                var oItem = oEvent.getSource().getSelectedItem().getBindingContext().getObject();
                this.bindItem(oItem);
            },
            /**
             * Called when the user changes a field of an item
             */
            onChangeItem: function (oEvent) {
                var that = this;
                // var oItem = oEvent.getSource().getBindingContext().getObject();
                var oModel = this.getOwnerComponent().getModel();
                oModel.setRefreshAfterChange(true);
                oModel.submitChanges({
                    success: function (oData, response) {
                        // oItem = oData ? oData.__batchResponses[2].data : oItem;
                        // that.getView().byId("itemStep").setValidated(oItem.Validated);
                        Fragment.byId(that.getView().createId("lineItems"), "itemTable").refreshAggregation("items");
                        Fragment.byId(that.getView().createId("lineItems"), "itemForm").getElementBinding().refresh();
                        // MessageToast.show("Changes were saved successfully");
                    },
                    error: function (oError) {
                        that.showErrorMessage(oError, "something went wrong while trying to update an item");
                    },
                });
            },
            onChangeMeins: function (oEvent) {
                var sMeins = oEvent.getSource().getSelectedKey();
                if (sMeins === "") sMeins = oEvent.getParameter("value");
                var sPath = oEvent.getSource().getBindingContext().getPath();
                var oMeinsComboBox = oEvent.getSource();
                this.getOwnerComponent().getModel().setProperty(sPath + "/Meins", sMeins);
                oMeinsComboBox.setSelectedKey(sMeins);
                this.onChangeItem(oEvent);
            },
            onChangeDeliveryDate: function (oEvent) {
                var sItemPath = Fragment.byId(this.getView().createId("lineItems"), "itemForm").getElementBinding().getPath();
                this.changeDate(sItemPath, oEvent);
                this.onChangeItem(oEvent);
            },
            /* =========================================================== */
            /* Action events Item             				               */
            /* =========================================================== */
            addItem: function () {
                var that = this;
                var sGuid = this.getOwnerComponent().getModel().getProperty(this.getView().getBindingContext().sPath + "/Guid");
                var oModel = this.getOwnerComponent().getModel();
                var oNewItem = {
                    Guid: sGuid,
                };
                AjaxHelper.postEntry(oModel, "/ItemSet", oNewItem).then(function (oData) {
                    // that.getView().byId("itemStep").setValidated(oData.Validated);
                }).catch(function (oError) {
                    that.showErrorMessage(oError, "something went wrong while trying to add a new item");
                });
            },
            onCopyItem: function (oEvent) {
                var that = this;
                var sGuid = this.getOwnerComponent().getModel().getProperty(this.getView().getBindingContext().sPath + "/Guid");
                var oModel = this.getOwnerComponent().getModel();
                var oNewItem = oEvent.getSource().getBindingContext().getObject();
                AjaxHelper.postEntry(oModel, "/ItemSet", oNewItem).then(function (oData) {
                    // that.getView().byId("itemStep").setValidated(oData.Validated);
                    Fragment.byId(that.getView().createId("lineItems"), "itemTable").refreshAggregation("items");
                }).catch(function (oError) {
                    that.showErrorMessage(oError, "something went wrong while trying to add a new item");
                });
            },
            onDeleteItem: function (oEvent) {
                var that = this;
                var oSelectedLine = oEvent.getSource().getParent();
                var sPath = oSelectedLine.getBindingContextPath();
                AjaxHelper.removeObject(this.getOwnerComponent().getModel(), sPath).then(function (oData) {
                    Fragment.byId(that.getView().createId("lineItems"), "itemTable").refreshAggregation("items");
                    //Fragment.byId(that.getView().createId("lineItems"), "itemForm").getBinding().refresh();
                }).catch(function (oError) {
                    that.showErrorMessage(oError, "something went wrong while trying to update an item");
                });
            },
            /**
             * Set an input's value with format: Name + (ID) (e.g. 'Proximus (1234)')
             */
            setInputValue: function (oInput, sId, sName) {
                if (sId !== undefined && sId !== null) {
                    if (sId !== "" || sName !== "") {
                        oInput.setValue(sName + " (" + sId + ")");
                    } else {
                        oInput.setValue("");
                    }
                    oInput.setValueState("None");
                } else {
                    oInput.setValueState("Error");
                }
            },
            /**
             * Get a single entity from a VL to display name of an object & validate the object
             */
            getValueListObject: function (sValueListPath, sId) {
                var that = this;
                var sPath = sValueListPath + "('" + sId + "')";
                var oObject = this.getOwnerComponent().getModel().getProperty(sPath);
                return new Promise(function (resolve, reject) {
                    if (sId === "") {
                        resolve();
                    } else if (oObject && oObject !== undefined) {
                        resolve(oObject);
                    } else {
                        AjaxHelper.readEntity(that.getOwnerComponent().getModel(), sPath, null, null, null).then(function (oData) {
                            resolve(oData);
                        }).catch(function (oError) {
                            // don't need messages popping up if user entered an invalid value in input field
                            that.getMessageManager().removeAllMessages();
                            reject(oError);
                        });
                    }
                });
            },
            /* =========================================================== */
            /* Internal methods        				                       */
            /* =========================================================== */
            _onObjectMatched: function (oEvent) {
                var that = this;
                var sPurchaseOrder = "";
                //  this.getView().byId("createpowizard").setCurrentStep("addninfo");
                if (oEvent) sPurchaseOrder = oEvent.getParameter("arguments").PurchaseOrder;
                this.getView().getModel("masterView").setProperty("/PurchaseOrder", sPurchaseOrder);
                if (!sPurchaseOrder || sPurchaseOrder === "0") {
                    // Create PO view
                    this.getSuggestionRowsInHeaderInfo();
                    var sTitle = this.getView().getModel("i18n").getResourceBundle().getText("title");
                    that.getView().byId("page").setTitle(sTitle);
                    if (!sPurchaseOrder) sPurchaseOrder === "";
                    this.getOwnerComponent().getModel().setRefreshAfterChange(true);
                    this.initGuid(this.getView(), false).then((oHeader) => {
                        if (oHeader.Action === "") {
                            oHeader.Action = "NEW";
                            that.getOwnerComponent().getModel().setProperty(that.getView().getElementBinding().getPath() + "/Action", oHeader.Action);
                        }
                        that.getView().getModel("masterView").setProperty("/action", oHeader.Action);
                        if (oHeader.OldEbeln !== "" && oHeader.Action === "MOD") {
                            that.onDiscard();
                            return;
                        }
                        that.getOwnerComponent().getModel().setProperty(that.getView().getElementBinding().getPath() + "/OldEbeln", "");
                        AjaxHelper.saveChanges(that.getOwnerComponent().getModel()).then((oData) => {
                            var oDraftHeader = oData ? oData.data.__batchResponses[1].data : oHeader;
                            // that.fetchVendorName(oDraftHeader);
                            if (oDraftHeader.VendorNr) that.setVendorInputValue(oDraftHeader);
                            if (oDraftHeader.Ekgrp) that.setEkgrpInputValue(oDraftHeader.Ekgrp);
                            if (oDraftHeader.Bukrs) that.setBukrsInputValue(oDraftHeader.Bukrs);
                            // that.fetchPurchaseGroupName(oDraftHeader);
                            // that.fetchCompanyCodeName(oDraftHeader);
                        }).catch((oError) => that.showErrorMessage(oError, "Something went wrong while retrieving the PO draft. Please refresh the page."));
                    }).catch((oError) => that.showErrorMessage(oError, "Something went wrong while retrieving the PO draft. Please refresh the page."));
                    that.getView().getModel("masterView").setProperty("/editable", true);
                    that.getView().getModel("masterView").setProperty("/displayMode", false);
                    that.getView().getModel("masterView").setProperty("/selectedLineItem", that.getView().getModel("masterView").getProperty("/lineItems")[0]);
                } else {
                    // Edit PO view
                    this.initGuid(this.getView(), true).then((oHeader) => {
                        var sTitle = this.getView().getModel("i18n").getResourceBundle().getText("titleDetail");
                        that.getView().byId("page").setTitle(sTitle);
                        that.getOwnerComponent().getModel().setProperty(that.getView().getElementBinding().getPath() + "/OldEbeln", sPurchaseOrder);
                        that.getView().getModel("masterView").setProperty("/action", "MOD");
                        that.getOwnerComponent().getModel().setProperty(that.getView().getElementBinding().getPath() + "/Action", "MOD");
                        that.getOwnerComponent().getModel().setProperty(that.getView().getElementBinding().getPath() + "/Discard", true);
                        // that.getOwnerComponent().getModel().setProperty(that.getView().getElementBinding().getPath() + "/CopyAtt", true);
                        AjaxHelper.saveChanges(that.getOwnerComponent().getModel()).then((oData) => {
                            that.getView().byId("createpowizard").setCurrentStep(that.getView().byId("additionalInfoStep"));
                            that.getView().byId('createpowizard').goToStep(that.getView().byId("headerStep"));
                            var oDraftHeader = oData ? oData.data.__batchResponses[1].data : oHeader;
                            if (oDraftHeader.VendorNr) that.setVendorInputValue(oDraftHeader);
                            if (oDraftHeader.Ekgrp) that.setEkgrpInputValue(oDraftHeader.Ekgrp);
                            if (oDraftHeader.Bukrs) that.setBukrsInputValue(oDraftHeader.Bukrs);
                            Fragment.byId(this.getView().createId("lineItems"), "itemTable").getBinding("items").refresh();
                            Fragment.byId(this.getView().createId("attachments"), "uploadCollection").getBinding("items").refresh();
                            Fragment.byId(this.getView().createId("attachments"), "businessDocumentCollection").getBinding("items").refresh();
                            that.getView().getModel("masterView").setProperty("/editable", false);
                            that.getView().getModel("masterView").setProperty("/displayMode", true);
                            that.getView().getModel("masterView").setProperty("/selectedLineItem", that.getView().getModel("masterView").getProperty("/lineItems")[0]);
                        }).catch((oError) => that.showErrorMessage(oError, "Something went wrong getting while retrieving the PO."));
                    }).catch((oError) => that.showErrorMessage(oError, "Something went wrong getting the Guid to make the Header Draft."));
                }
            },
            hideReviewButton: function () {
                // last step contains a review button which should be hidden
                this.getView().byId("createpowizard").setShowNextButton(false);
            },
            saveChanges: function (oEvent) {
                AjaxHelper.saveChanges(this.getOwnerComponent().getModel());
            },
            bindItem: function (oItem) {
                var sPath = this.getOwnerComponent().getModel().createKey("/ItemSet", {
                    Guid: oItem.Guid,
                    Ebelp: oItem.Ebelp,
                });
                var oForm = Fragment.byId(this.getView().createId("lineItems"), "itemForm");
                oForm.bindElement({
                    path: sPath,
                });
                // this.byId("itemStep").setValidated(oItem.Validated);
            },
            onUpdateFinished: function (oEvent) {
                var oItem;
                if (oEvent.getSource().getSelectedItem()) {
                    oItem = oEvent.getSource().getSelectedItem().getBindingContext().getObject();
                } else {
                    if (oEvent.getSource().getItems().length > 0) {
                        oItem = oEvent.getSource().getItems()[0].getBindingContext().getObject();
                        oEvent.getSource().getItems()[0].setSelected(true);
                    }
                }
                if (oItem !== undefined)
                    this.bindItem(oItem);
            },
            changeDate: function (sGlobalEntityPath, oEvent) {
                var oDate = moment.parseZone(oEvent.getSource().getDateValue()).utc(true).startOf("day").toDate(); // The date from the DatePicker control is in local time. We convert it to GMT standard time with the utc(true)
                if (!moment(oDate).isValid()) {
                    oDate = null;
                }
                var sPropertyPath = oEvent.getSource().getBinding("value").getPath();
                this.getOwnerComponent().getModel().setProperty(sGlobalEntityPath + "/" + sPropertyPath, oDate);
            },
            onDisplayMode: function () {
                var that = this;
                this.getSuggestionRowsInHeaderInfo();
                this.getSuggestionRowsInItemsWizardStep();
                var oHeader = this.getView().getBindingContext().getObject();
                if (oHeader.NewEbeln) {
                    this.initGuid(this.getView()).then(function (oNewHeader) {
                        // var sPath = that.getOwnerComponent().getModel().createKey("/HeaderSet", {
                        //     Guid: oNewHeader.Guid
                        // });
                        var sPath = that.getHeaderSetPath(oNewHeader.Guid);
                        that.getOwnerComponent().getModel().setProperty(sPath + "/OldEbeln", oHeader.NewEbeln);
                        that.getView().getModel("masterView").setProperty("/action", "MOD");
                        that.getOwnerComponent().getModel().setProperty(sPath + "/Action", "MOD");
                        that.getOwnerComponent().getModel().setProperty(sPath + "/Discard", true);
                        AjaxHelper.saveChanges(that.getOwnerComponent().getModel()).then(function () {
                            Fragment.byId(that.getView().createId("lineItems"), "itemTable").getBinding("items").refresh();
                            Fragment.byId(that.getView().createId("attachments"), "uploadCollection").getBinding("items").refresh();
                            Fragment.byId(that.getView().createId("attachments"), "businessDocumentCollection").getBinding("items").refresh();
                            that.getView().getModel("masterView").refresh();
                        });
                    })
                }
                this.getView().getModel("masterView").setProperty("/editable", !this.getView().getModel("masterView").getProperty("/editable"));
                this.getView().getModel("masterView").setProperty("/displayMode", !this.getView().getModel("masterView").getProperty("/displayMode"));
                if (this.getView().getModel("masterView").getProperty("/editable")) {
                    var sTitle = this.getView().getModel("i18n").getResourceBundle().getText("titleEdit");
                } else {
                    var sTitle = this.getView().getModel("i18n").getResourceBundle().getText("titleDetail");
                }
                this.getView().byId("page").setTitle(sTitle);
            },
            onCopyPO: function (oEvent) {
                var that = this;
                var oModel = this.getOwnerComponent().getModel();
                var oHeader = this.getView().getBindingContext().getObject();
                sap.m.MessageBox.confirm("Do you want to copy the attachments from the previous Purchase order?", {
                    actions: [sap.m.MessageBox.Action.CANCEL, MessageBox.Action.YES, MessageBox.Action.NO],
                    onClose: function (sAnswer) {
                        if (sAnswer === "YES" || sAnswer === "NO") {
                            var sTitle = that.getView().getModel("i18n").getResourceBundle().getText("title");
                            that.getView().byId("page").setTitle(sTitle);
                            if (oHeader.NewEbeln) {
                                that.initGuid(that.getView()).then(function (oNewHeader) {
                                    // var sPath = oModel.createKey("/HeaderSet", {
                                    //     Guid: oNewHeader.Guid
                                    // });
                                    var sPath = that.getHeaderSetPath(oNewHeader.Guid);
                                    if (sAnswer === "YES") {
                                        oModel.setProperty(sPath + "/Action", "COPY_ATT");
                                        that.getView().getModel("masterView").setProperty("/action", "COPY_ATT");
                                    } else {
                                        oModel.setProperty(sPath + "/Action", "COPY");
                                        that.getView().getModel("masterView").setProperty("/action", "COPY");
                                    }
                                    // that.getView().getModel("masterView").setProperty("/action", "COPY");
                                    // oModel.setProperty(sPath + "/Action", "COPY");
                                    oModel.setProperty(sPath + "/OldEbeln", oHeader.NewEbeln);
                                    oModel.setProperty(sPath + "/Discard", true);
                                    AjaxHelper.saveChanges(oModel).then(function (oData, oResponse) {
                                        Fragment.byId(that.getView().createId("lineItems"), "itemTable").getBinding("items").refresh();
                                        Fragment.byId(that.getView().createId("attachments"), "uploadCollection").getBinding("items").refresh();
                                        Fragment.byId(that.getView().createId("attachments"), "businessDocumentCollection").getBinding("items").refresh();
                                        AttachmentHelper.refreshAttachmentList();
                                        that.getView().getModel("masterView").refresh();
                                    });
                                })
                            } else {
                                // that.getView().getModel("masterView").setProperty("/action", "COPY");
                                // oModel.setProperty(that.getView().getElementBinding().getPath() + "/Action", "COPY");
                                if (sAnswer === "YES") {
                                    oModel.setProperty(that.getView().getElementBinding().getPath() + "/Action", "COPY_ATT");
                                    that.getView().getModel("masterView").setProperty("/action", "COPY_ATT");
                                } else {
                                    oModel.setProperty(that.getView().getElementBinding().getPath() + "/Action", "COPY");
                                    that.getView().getModel("masterView").setProperty("/action", "COPY");
                                }
                                oModel.setProperty(that.getView().getElementBinding().getPath() + "/Discard", true);
                                AjaxHelper.saveChanges(oModel).then(function () {
                                    Fragment.byId(that.getView().createId("lineItems"), "itemTable").refreshAggregation("items");
                                    Fragment.byId(that.getView().createId("attachments"), "uploadCollection").getBinding("items").refresh();
                                    Fragment.byId(that.getView().createId("attachments"), "businessDocumentCollection").getBinding("items").refresh();
                                    AttachmentHelper.refreshAttachmentList();
                                    that.getView().getModel("masterView").refresh();
                                });
                            }
                            that.getView().getModel("masterView").setProperty("/editable", true);
                        } else {
                            var sTitle = that.getView().getModel("i18n").getResourceBundle().getText("titleDetail");
                            that.getView().byId("page").setTitle(sTitle);
                            return false;
                        }
                    }
                });
            },
            /* =========================================================== */
            /* PO History           				                       */
            /* =========================================================== */
            onPOHistory: function () {
                if (!this._oDialogPOHistory) {
                    this._oDialogPOHistory = sap.ui.xmlfragment(this.fragmentIdPOHistory, "be.amista.createpo.fragments.poHistory", this);
                    this.getView().addDependent(this._oDialogPOHistory);
                }
                this._oDialogPOHistory.open();
                this.onHistoryTableUpdateFinished();
            },
            closePOHistoryDialog: function () {
                this._oDialogPOHistory.close();
            },
            onHistoryTableUpdateFinished: function (oEvent) {
                var that = this;
                var oModel = this.getOwnerComponent().getModel();
                var sItemPath = Fragment.byId(this.getView().createId("lineItems"), "itemTable").getSelectedItem().getBindingContext().getPath();
                AjaxHelper.readEntity(oModel, sItemPath + "/to_History").then(function (oData) {
                    if (!oData.results || oData.results.length === 0) return;
                    var aHistoryGR = [],
                        aHistoryIR = [];
                    var fTotalGRMenge = 0,
                        fTotalIRMenge = 0,
                        fTotalGRDmbtr = 0,
                        fTotalIRDmbtr = 0,
                        fTotalGRWrbtr = 0,
                        fTotalIRWrbtr = 0;
                    oData.results.forEach(function (oItem) {
                        oItem.Budat = formatter.formatDateForView(oItem.Budat);
                        if (oItem.Wepos) {
                            aHistoryGR.push(oItem);
                            fTotalGRMenge += parseFloat(oItem.Menge);
                            fTotalGRDmbtr += parseFloat(oItem.Dmbtr);
                            fTotalGRWrbtr += parseFloat(oItem.Wrbtr);
                        }
                        if (oItem.Repos) {
                            aHistoryIR.push(oItem);
                            fTotalIRMenge += parseFloat(oItem.Menge);
                            fTotalIRDmbtr += parseFloat(oItem.Dmbtr);
                            fTotalIRWrbtr += parseFloat(oItem.Wrbtr);
                        }
                    });
                    that.getView().getModel("masterView").setProperty("/historyGR", aHistoryGR);
                    that.getView().getModel("masterView").setProperty("/historyIR", aHistoryIR);
                    that.getView().getModel("masterView").setProperty("/TotalGRMenge", fTotalGRMenge.toFixed(2));
                    that.getView().getModel("masterView").setProperty("/TotalIRMenge", fTotalIRMenge.toFixed(2));
                    that.getView().getModel("masterView").setProperty("/TotalGRDmbtr", fTotalGRDmbtr.toFixed(2));
                    that.getView().getModel("masterView").setProperty("/TotalIRDmbtr", fTotalIRDmbtr.toFixed(2));
                    that.getView().getModel("masterView").setProperty("/TotalGRWrbtr", fTotalGRWrbtr.toFixed(2));
                    that.getView().getModel("masterView").setProperty("/TotalIRWrbtr", fTotalIRWrbtr.toFixed(2));
                    that.getView().getModel("masterView").setProperty("/GRHswae", aHistoryGR[0].Hswae ? aHistoryGR[0].Hswae : "");
                    that.getView().getModel("masterView").setProperty("/IRHswae", aHistoryIR[0].Hswae ? aHistoryGR[0].Hswae : "");
                    that.getView().getModel("masterView").setProperty("/GRMeins", aHistoryGR[0].Meins ? aHistoryGR[0].Meins : "");
                    that.getView().getModel("masterView").setProperty("/IRMeins", aHistoryIR[0].Meins ? aHistoryGR[0].Meins : "");
                    that.getView().getModel("masterView").setProperty("/GRWaers", aHistoryGR[0].Waers ? aHistoryGR[0].Waers : "");
                    that.getView().getModel("masterView").setProperty("/IRWaers", aHistoryIR[0].Waers ? aHistoryGR[0].Waers : "");
                }).catch(function (oError) {
                    that.showErrorMessage(oError);
                });
            },
            onHistoryAttachmentsPress: function (oEvent) {
                var oHistoryDocument = oEvent.getSource().getBindingContext("masterView").getObject();
                var sPath = this.getOwnerComponent().getModel().createKey("/HistorySet", {
                    Guid: oHistoryDocument.Guid,
                    Mblnr: oHistoryDocument.Mblnr,
                    Mblpo: oHistoryDocument.Mblpo,
                    Ebelp: oHistoryDocument.Ebelp
                });
                if (!this._oDialogPOHistoryAttachments) {
                    this._oDialogPOHistoryAttachments = sap.ui.xmlfragment(this.fragmentIdPOHistoryAttachments, "be.amista.createpo.fragments.poHistoryAttachments", this);
                    this.getView().addDependent(this._oDialogPOHistoryAttachments);
                }
                this.bindHistoryAttachments(sPath);
                this._oDialogPOHistoryAttachments.open();
            },
            bindHistoryAttachments: function (sPath) {
                var oHistoryUploadCollection = Fragment.byId(this.fragmentIdPOHistoryAttachments, "historyAttachmentCollection");
                if (oHistoryUploadCollection.getBindingInfo("items")) {
                    var oTemplate = oHistoryUploadCollection.getBindingInfo("items").template;
                } else {
                    var oTemplate = oHistoryUploadCollection.getItems()[0].clone();
                }
                oHistoryUploadCollection.bindItems({
                    path: sPath + "/to_Attachments",
                    template: oTemplate,
                });
            },
            onHistoryAttachmentDownload: function (oEvent) {
                var oItem = oEvent.getSource().getParent();
                var oHistoryUploadCollection = oItem.getParent();
                oHistoryUploadCollection.downloadItem(oItem, true);
                var sId = oItem.getDocumentId();
                var oAtt = oEvent.getSource().getBindingContext().getObject();
                var sObjectPath = this.getOwnerComponent().getModel().createKey("HistoryAttachmentFileSet", {
                    Guid: oAtt.Guid,
                    Mblnr: oAtt.Mblnr,
                    Mblpo: oAtt.Mblpo,
                    Ebelp: oAtt.Ebelp,
                    AttachmentId: oAtt.AttachmentId
                });
                var uri = this.getVar("Uri");
                var sReadPath = uri + sObjectPath + "/$value";
                if (oItem) {
                    oItem.setUrl(sReadPath);
                    oHistoryUploadCollection.downloadItem(oItem, true);
                    oItem.setUrl(null);
                }
            },
            closePOHistoryAttachmentsDialog: function () {
                this._oDialogPOHistoryAttachments.close();
            },
            onClosePreview: function () {
                this._oDialogPreview.close();
            },
            // onAcccatgiven: function (oEvent) {
            //     this.getView().getModel("masterView").setProperty("/selectedLineItem/lineItemDetails/accountAssignmentCategory",
            //         oEvent.getSource().getProperty("value"));
            // },
            /* =========================================================== */
            /* Footer actions        				                       */
            /* =========================================================== */
            onSave: function (oEvent) {
                this.submitPO(false).then().catch();
            },
            onHold: function (oEvent) {
                this.submitPO(true).then().catch();
            },
            onDiscard: function (oEvent) {
                var that = this
                this.getView().byId("createpowizard").setCurrentStep(this.getView().byId("headerStep"));
                this.getView().byId('createpowizard').goToStep(this.getView().byId("headerStep"));
                this.getView().byId('createpowizard').setShowNextButton(true);
                this.getOwnerComponent().getModel().setProperty(this.getView().getElementBinding().getPath() + "/OldEbeln", "");
                this.getView().getModel("masterView").setProperty("/action", "NEW");
                this.getOwnerComponent().getModel().setProperty(this.getView().getElementBinding().getPath() + "/Action", "NEW");
                this.getOwnerComponent().getModel().setProperty(this.getView().getElementBinding().getPath() + "/Discard", true);
                AjaxHelper.saveChanges(this.getOwnerComponent().getModel()).then(oData => {
                    // that.addItem(); done in back-end, as this was not always happening when needed from front-end
                    // clear input fields
                    var oHeader = oData.data.__batchResponses[1].data;
                    that.setVendorInputValue(oHeader);
                    that.setEkgrpInputValue(oHeader.Ekgrp);
                    that.setBukrsInputValue(oHeader.Bukrs);
                    Fragment.byId(that.getView().createId("lineItems"), "itemTable").getBinding("items").refresh();
                    Fragment.byId(that.getView().createId("attachments"), "uploadCollection").getBinding("items").refresh();
                    Fragment.byId(that.getView().createId("attachments"), "businessDocumentCollection").getBinding("items").refresh();
                    that.getRouter().navTo("Create", {}, true);
                }).catch(oError => that.showErrorMessage(oError, "Something went wrong discarding the draft"));
            },
            submitPO: function (bHold) {
                return new Promise((resolve, reject) => {
                    var that = this;
                    // to prevent multiple submits, this is much faster than using busy indicator of local model
                    sap.ui.core.BusyIndicator.show(0);
                    var sGuid = this.getOwnerComponent().getModel().getProperty(this.getView().getElementBinding().getPath()).Guid;
                    var oData = {
                        Guid: sGuid
                    };
                    if (bHold) oData.Hold = true;
                    // remove previous messages before submit
                    this.getMessageManager().removeAllMessages();
                    this.getOwnerComponent().getModel().create("/HeaderSet", oData, {
                        success: oCreatedPO => {
                            sap.ui.core.BusyIndicator.hide();
                            if (oCreatedPO.NewEbeln === "") {
                                if (bHold) {
                                    sap.m.MessageBox.show("Something went wrong when saving the PO");
                                }
                                reject();
                                return;
                            }
                            AttachmentHelper.refreshAttachmentList();
                            that.getView().getModel("masterView").setProperty("/editable", false);
                            that.getView().getModel("masterView").setProperty("/displayMode", true);
                            that.storeVar("Guid", "0");
                            resolve(oCreatedPO);
                        },
                        error: oError => {
                            // if (bHold) {
                            that.showErrorMessage(oError, "Something went wrong when saving the PO");
                            // }
                            reject(oError);
                        }
                    });
                });
            },
            // We need to implement the suggestion values in the controller instead of in the view because this way, we can split up the call.
            // If we implemented everything in the view, this was all in 1 batch call that was taking more than 6 seconds to load.
            // In this case, we split up this time into 2 main parts
            getSuggestionRowsInHeaderInfo: function () {
                var that = this;
                var aAllPromises = [];

                aAllPromises.push(AjaxHelper.readEntity(this.getOwnerComponent().getModel(), "/VL_VendorSet", null, null, {
                    $select: 'VendorNr,Name'
                }));
                aAllPromises.push(AjaxHelper.readEntity(this.getOwnerComponent().getModel(), "/C_PurchasingGroupValueHelp", null, null, {
                    $select: 'PurchasingGroup,PurchasingGroupName'
                }));
                aAllPromises.push(AjaxHelper.readEntity(this.getOwnerComponent().getModel(), "/C_MM_CompanyCodeValueHelp", null, null, {
                    $select: 'CompanyCode,CompanyCodeName'
                }));
                sap.ui.core.BusyIndicator.show(0);
                Promise.all(aAllPromises).then((values) => {
                    that.getView().getModel("masterView").setProperty("/VL_VendorSet", values[0].results);
                    that.getView().getModel("masterView").setProperty("/C_PurchasingGroupValueHelp", values[1].results);
                    that.getView().getModel("masterView").setProperty("/C_MM_CompanyCodeValueHelp", values[2].results);
                    sap.ui.core.BusyIndicator.hide();

                }).catch(function (oError) {
                    that.showErrorMessage(oError);
                    sap.ui.core.BusyIndicator.hide();
                });
            },
            getSuggestionRowsInItemsWizardStep: function () {
                var that = this;
                var aAllPromises = [];

                aAllPromises.push(AjaxHelper.readEntity(this.getOwnerComponent().getModel(), "/C_MM_MaterialGroupValueHelp", null, null, {
                    $select: 'MaterialGroup,MaterialGroupName'
                }));
                aAllPromises.push(AjaxHelper.readEntity(this.getOwnerComponent().getModel(), "/C_MM_CostCenterValueHelp", null, null, {
                    $select: 'CostCenter,CostCenterName'
                }));
                aAllPromises.push(AjaxHelper.readEntity(this.getOwnerComponent().getModel(), "/VL_InternalOrderSet", null, null, {
                    $select: 'Aufnr,Ktext'
                }));
                aAllPromises.push(AjaxHelper.readEntity(this.getOwnerComponent().getModel(), "/VL_TaxCodeSet", null, null, {
                    $select: 'Mwskz,Text1'
                }));
                aAllPromises.push(AjaxHelper.readEntity(this.getOwnerComponent().getModel(), "/C_MM_AccountAssignCatValueHelp", null, null, {
                    $select: 'AccountAssignmentCategory,AcctAssignmentCategoryName'
                }));
                sap.ui.core.BusyIndicator.show(0);
                Promise.all(aAllPromises).then((values) => {
                    that.getView().getModel("masterView").setProperty("/C_MM_MaterialGroupValueHelp", values[0].results);
                    that.getView().getModel("masterView").setProperty("/C_MM_CostCenterValueHelp", values[1].results);
                    that.getView().getModel("masterView").setProperty("/VL_InternalOrderSet", values[2].results);
                    that.getView().getModel("masterView").setProperty("/VL_TaxCodeSet", values[3].results);
                    that.getView().getModel("masterView").setProperty("/C_MM_AccountAssignCatValueHelp", values[4].results);
                    sap.ui.core.BusyIndicator.hide();
                }).catch(oError => {
                    that.showErrorMessage(oError)
                    sap.ui.core.BusyIndicator.hide();
                });
            },
            onLineItemsStepActive: function (oEvent) {
                this.getSuggestionRowsInItemsWizardStep();
            },
            // fetchVendorName: function (oHeader) {
            //     this.fetchDisplayNameOfObject("/VL_VendorSet", oHeader.VendorNr, "/selectedVendor",
            //         "Something went wrong getting the Vendor Name"
            //     );
            // },
            // fetchPurchaseGroupName: function (oHeader) {
            //     this.fetchDisplayNameOfObject("/VL_PurchasingGroupSet", oHeader.Ekgrp, "/selectedPurchaseGroup",
            //         "Something went wrong getting the Purchase Group Name"
            //     );
            // },
            // fetchCompanyCodeName: function (oHeader) {
            //     this.fetchDisplayNameOfObject("/VL_CompanyCodeSet", oHeader.Bukrs, "/selectedCompanyCode",
            //         "Something went wrong getting the Company Name"
            //     );
            // },
            // fetchDisplayNameOfObject: function (sVLPath, sId, sPropertyPath, sErrorMessage) {
            //     var that = this;
            //     if (sId !== "") {
            //         var oObject = this.getOwnerComponent().getModel().getProperty(sVLPath + "('" + sId + "')");
            //         if (oObject === undefined) {
            //             this.getOwnerComponent().getModel().read(sVLPath + "('" + sId + "')", {
            //                 success: function (oData, oResponse) {
            //                     that.getView().getModel("masterView").setProperty(sPropertyPath, oData);
            //                 },
            //                 error: function (oError) {
            //                     that.showErrorMessage(oError, sErrorMessage);
            //                 },
            //             });
            //         } else {
            //             this.getView().getModel("masterView").setProperty(sPropertyPath, oObject);
            //         }
            //     }
            // },
            /* =========================================================== */
            /* ValueListHelper methods				                       */
            /* =========================================================== */
            onFieldHelpRequest: function (sTextId, oBtn) {
                ValueListHelper.fieldHelpRequest(sTextId, oBtn);
            },
            onCloseFieldHelp: function () {
                ValueListHelper.closeFieldHelp();
            },
            valueHelpRequestVendor: function () {
                ValueListHelper.valueHelpRequestVendor();
            },
            valueHelpConfirmVendor: function (oEvent) {
                ValueListHelper.valueHelpConfirmVendor(oEvent);
            },
            valueHelpRequestPurGrp: function () {
                ValueListHelper.valueHelpRequestPurGrp();
            },
            valueHelpConfirmPurGrp: function (oEvent) {
                ValueListHelper.valueHelpConfirmPurGrp(oEvent);
            },
            valueHelpRequestCC: function () {
                ValueListHelper.valueHelpRequestCC();
            },
            valueHelpConfirmCC: function (oEvent) {
                ValueListHelper.valueHelpConfirmCC(oEvent);
            },
            valueHelpRequestAcct: function () {
                ValueListHelper.valueHelpRequestAcct();
            },
            valueHelpConfirmAcct: function (oEvent) {
                ValueListHelper.valueHelpConfirmAcct(oEvent);
            },
            valueHelpRequestInternalOrder: function () {
                ValueListHelper.valueHelpRequestInternalOrder();
            },
            valueHelpConfirmInternalOrder: function (oEvent) {
                ValueListHelper.valueHelpConfirmInternalOrder(oEvent);
            },
            valueHelpRequestCostCenter: function () {
                ValueListHelper.valueHelpRequestCostCenter();
            },
            valueHelpConfirmCostCenter: function (oEvent) {
                ValueListHelper.valueHelpConfirmCostCenter(oEvent);
            },
            valueHelpRequestWBS: function () {
                ValueListHelper.valueHelpRequestWBS();
            },
            valueHelpRequestMatGrp: function () {
                ValueListHelper.valueHelpRequestMatGrp();
            },
            valueHelpConfirmMatGrp: function (oEvent) {
                ValueListHelper.valueHelpConfirmMatGrp(oEvent);
            },
            valueHelpRequestAnln1: function () {
                ValueListHelper.valueHelpRequestAnln1();
            },
            valueHelpConfirmAnln1: function (oEvent) {
                ValueListHelper.valueHelpConfirmAnln1(oEvent);
            },
            valueHelpRequestTaxCode: function () {
                ValueListHelper.valueHelpRequestTaxCode();
            },
            valueHelpConfirmTaxCode: function (oEvent) {
                ValueListHelper.valueHelpConfirmTaxCode(oEvent);
            },
            /* =========================================================== */
            /* AttachmentHelper methods				                       */
            /* =========================================================== */
            onChangeAttachment: function (oEvent) {
                AttachmentHelper.onChangeAttachment(oEvent);
            },
            onChangeBusinessDocument: function (oEvent) {
                AttachmentHelper.onChangeBusinessDocument(oEvent);
            },
            onBeforeUpload: function (oEvent) {
                AttachmentHelper.onBeforeUpload(oEvent);
            },
            handleFileUpload: function (oEvent) {
                AttachmentHelper.handleFileUpload(oEvent);
            },
            handleFileDownload: function (oEvent) {
                AttachmentHelper.handleFileDownload(oEvent);
            },
            handleTypeMissmatch: function (oEvent) {
                AttachmentHelper.handleTypeMissmatch(oEvent);
            },
            handleAttachmentDelete: function (oEvent) {
                AttachmentHelper.handleAttachmentDelete(oEvent);
            },
            handlefileRenamed: function (oEvent) {
                AttachmentHelper.handlefileRenamed(oEvent);
            },
            onAttachmentsReceived: function (oEvent) {
                AttachmentHelper.onAttachmentsReceived(oEvent);
            },
            onPreview: function (oEvent) {
                var oAttachment = oEvent.getSource().getParent();
                AttachmentHelper.onPreview(oAttachment);
            },
            onPOPreview: function (oEvent) {
                AttachmentHelper.onPOPreview();
            },
            previewPO: function (oEvent) {
                AttachmentHelper.previewPO();
            },
        });
    });