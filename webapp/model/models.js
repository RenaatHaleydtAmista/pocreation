sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/Device"
], function (JSONModel, Device) {
    "use strict";

    return {

        createDeviceModel: function () {
            var oModel = new JSONModel(Device);
            oModel.setDefaultBindingMode("OneWay");
            return oModel;
        },
        createLocalModel: function () {
            var oModel = new JSONModel({
                busy: true,
                Guid: "0",
                Validated: false,
                User: null,
                fileTypes: ["pdf", "png", "jpg", "jpeg", "xls", "xlsx", "msg", "eml", "doc", "docx"]
            });
            return oModel;
        },

    };
});