sap.ui.define(["sap/ui/core/format/FileSizeFormat", "sap/ui/core/format/NumberFormat"], function (FileSizeFormat, NumberFormat) {
    "use strict";

    return {
        /**
         * Rounds the number unit value to 2 digits
         * @public
         * @param {string} sValue the number string to be rounded
         * @returns {string} sValue with 2 digits rounded
         */
        numberUnit: function (sValue) {
            if (!sValue) {
                return "";
            }
            return parseFloat(sValue).toFixed(2);
        },
        currencyThousandSeparator: function (sValue) {
            var oFormat = sap.ui.core.format.NumberFormat.getFloatInstance({
                "groupingEnabled": true,
                "groupingSeperator": ".",
                "groupingSize": 3,
                "decimalSeparator": ","
            });
            return oFormat.format(sValue);
        },
        //Basic formatter stuff
        /**
         * Rounds the currency value to 2 digits
         *
         * @public
         * @param {string} sValue value to be formatted
         * @returns {string} formatted currency value with 2 digits
         */
        currencyValue: function (sValue) {
            if (!sValue) {
                return "";
            }

            return parseFloat(sValue).toFixed(2);
        },
        convertTrueFalseToYesNo: function (bValue) {
            return bValue ? 'Yes' : 'No';
        },
        formatDateForView: function (oValue) {
            if (typeof oValue === "string")
                oValue = this.formatter.convertEpochDateToJavaScriptDate(oValue);
            if (typeof oValue === "object") {
                var oDateFormat = sap.ui.core.format.DateFormat.getDateInstance({
                    pattern: "dd/MM/YYYY",
                });
                return oDateFormat.format(oValue);
            }
        },
        /**
         * This method requires a name of a property to find it in the CARModel.
         * This method converts a date with the format ‘dd-MM-YYYY’ into a Javascript Date Object
         * @param propName
         * @returns String
         */
        convertDateToJavaScriptDateObject: function (sDate) {
            if (sDate === null) return "";

            if (this.isValidDate(sDate) || sDate.includes("/Date")) return sDate;

            if (sDate && sDate !== "") {
                var day = sDate.split("-")[0];
                var month = parseInt(sDate.split("-")[1].split("-")[0], 10) - 1;
                var year = sDate.slice(-4);
                return new Date(year, month, day);
            }

            return "";
        },
        /**
         * Checks if a date is an instance of Date and different from NaN
         * @param d
         * @returns boolean
         */
        isValidDate: function (d) {
            return d instanceof Date && !isNaN(d);
        },
        /**
         * Converts the Epoch date (the date coming from HANA) into the format ‘dd-MM-YYYY’ to display in the views
         * @param dateEpoch
         * @returns String
         */
        convertEpochDateToDateWithDayDashMonthDashYear: function (dateEpoch) {
            var dateJS;
            var sDateJS = "";

            dateEpoch = dateEpoch.split("(")[1];
            dateEpoch = dateEpoch.split(")")[0];
            if (dateEpoch !== "0") {
                dateJS = new Date(parseInt(dateEpoch, 10)); // new Date() is automaticcaly converted to the local time zone (with time offset)
                // var signDateOffset = dateJS.getTimezoneOffset().toString();
                // if (signDateOffset.startsWith("-")) {
                // 	dateJS.setMinutes(parseInt(signDateOffset.split("-")[1], 10));
                // }
                // if (signDateOffset.startsWith("+")) {
                // 	dateJS.setMinutes(-parseInt(signDateOffset.split("-")[1], 10));
                // }

                sDateJS =
                    dateJS.getDate() +
                    "-" +
                    (dateJS.getUTCMonth() + 1) +
                    "-" +
                    dateJS.getUTCFullYear();
            }
            return sDateJS;
        },
        /**
         * Converts the Epoch date (the data coming from HANA) into a Javascript Date object
         * @param dateEpoch
         * @returns Javascript Date Object
         */
        convertEpochDateToJavaScriptDate: function (dateEpoch) {
            var dateJS;

            dateEpoch = dateEpoch.split("(")[1];
            dateEpoch = dateEpoch.split(")")[0];
            if (dateEpoch !== "0") {
                if (dateEpoch.includes("+"))
                    // When the dates are coming from sHaRe (SuccessFactors), it is possible it looks like /Date(1586981513000+0000)/ for example
                    dateEpoch = dateEpoch.split("+")[0];
                dateJS = new Date(parseInt(dateEpoch, 10)); // new Date() is automaticcaly converted to the local time zone (with time offset)
            }
            return dateJS;
        },
        /**
         * Converts the Javascript Date Object into a Date with format ‘dd-MM-YYY’ to display in the front.
         * @param date
         * @returns String
         */
        _convertJavascriptDateToDateWithDashes: function (date) {
            var sDate = "";
            var day, month, year;
            if (this.isValidDate(date)) {
                day = date.toISOString().split("T")[0].split("-")[2];
                month = date.toISOString().split("T")[0].split("-")[1];
                year = date.toISOString().split("T")[0].split("-")[0];
                sDate = day + "-" + month + "-" + year;
            }
            return sDate;
        },
        /**
         * Returns the conversion of the DateTime object into epoch date
         * @constructor
         * @param dateTime
         * @returns String
         */
        _convertDateTimeIntoEpoch: function (dateTime) {
            // var dateJS = new Date(dateTime.split("T")[0]);
            var dateJS = dateTime;
            if (this.isValidDate(dateJS) && dateJS !== "") {
                return "/Date(" + Date.parse(dateTime) + ")/";
            } else {
                return "/Date(" + "0" + ")/";
            }
        },
        /**
         * Returns the modified Error object with the error message split into an array of lines
         * @param oError
         * @returns Error object
         */
        compressErrorObject: function (oError) {
            if (oError.responseText !== undefined) {
                try {
                    var oErrorText = JSON.parse(oError.responseText);
                    oError.responseText = oErrorText;
                } catch (e) {
                    oError.responseText = this.makeNewLineAfterXChars(
                        oError.responseText
                    );
                }
            }
            if (oError.responseJSON !== undefined) {
                if (oError.responseJSON.error.message.value !== undefined)
                    oError.responseJSON.error.message.value = this.makeNewLineAfterXChars(
                        oError.responseJSON.error.message.value
                    );
            }
            if (typeof oError === "object" && JSON.stringify(oError) === "{}") {
                // oError is a TypeError comming from the frontend
                oError = String(oError);
            }

            return oError;
        },
        /**
         * Returns an array of lines (with X words)
         * @param sString
         * @returns Array
         */
        makeNewLineAfterXChars: function (sString) {
            var aChunks = [];
            var aTemp = [];
            var aTempChunk = "";

            aTemp = sString.split(" ");

            for (var i = 0; i < aTemp.length; i++) {
                aTempChunk += aTemp[i] + " ";
                if (
                    aTempChunk.length > 50 ||
                    (i === aTemp.length - 1 && aTempChunk.length !== 0)
                ) {
                    //We make the lines 50 words long and check if the last aTempChunk is empty before we go out of the loop
                    aChunks.push(aTempChunk);
                    aTempChunk = "";
                }
            }
            return aChunks;
        },
        formatDateFormatDependingOnLanguageSettings: function (sLanguage) {
            switch (sLanguage) {
                case "en":
                    return "MM/dd/yyyy";
                case "es":
                    return "d/MM/yyyy";
                case "de":
                case "fr":
                case "in":
                case "pl":
                case "pt":
                case "ru":
                    return "dd/MM/yyyy";
                case "nl":
                    return "d-M-yyyy";
                case "zh":
                    return "yyyy-M-d";
            }
            return "MM/dd/yyyy";
        },
        formatDateDependingOnLanguageSettings: function (dDate, sDateFormat) {
            if (dDate) {
                return moment(dDate).format(sDateFormat);
            }
            return null;
        },
        displayyyyyMMddDate: function (sDate) {
            var oDateParser = sap.ui.core.format.DateFormat.getDateInstance({
                format: "yyyyMMdd",
            });

            if (!isNaN(parseInt(sDate))) {
                // The original date is a string, but can be parsed to an Integer. Like for example: 20201223
                var oDate = oDateParser.parse(sDate);
                return this.formatter.formatJavascriptDateObjectIntoddMMyyyy(
                    oDate,
                    this
                );
            } else {
                if (sDate === "T.B.C") {
                    return sDate;
                } else {
                    return null;
                }
            }
        },
        formatJavascriptDateObjectIntoddMMyyyy: function (oDate, oContext) {
            var sLanguage = oContext
                .getModel("worklistView")
                .getProperty("/UserLanguage");
            var oDateFormatter = sap.ui.core.format.DateFormat.getDateInstance({
                pattern: oContext.formatter.formatDateFormatDependingOnLanguageSettings(
                    sLanguage
                ),
            });
            return oDateFormatter.format(oDate);
        },
        displayCorrectTime: function (sTime) {
            var oTimeParser = sap.ui.core.format.DateFormat.getTimeInstance({
                format: "hhmmss",
            });
            if (sTime) {
                var oTime = oTimeParser.parse(sTime);
                return this.formatter.formatJavascriptTimeObjectIntohhmmss(oTime);
            } else {
                return null;
            }
        },
        formatJavascriptTimeObjectIntohhmmss: function (oTime) {
            var oTimeFormatter = sap.ui.core.format.DateFormat.getTimeInstance({
                pattern: "hh:mm:ss",
            });
            return oTimeFormatter.format(oTime);
        },
        formatSize: function (sValue) {
            if (jQuery.isNumeric(sValue)) {
                return FileSizeFormat.getInstance({
                    binaryFilesize: false,
                    maxFractionDigits: 1,
                    maxIntegerDigits: 3,
                }).format(sValue);
            } else {
                return sValue;
            }
        },
        statusIcon: function (oEndDate, sStatusCode) {
            // We show the success icon if the EndDate is after today AND the status is NOT completed, Canceled or rejected => means the user has still time to fix this problem
            // We show the warning icon if the EndDate is today AND the status is NOT completed, Canceled or rejected => means the user need to fix the problem today
            // We show the error icon if the EndDate is before today AND the status is NOT completed, Canceled or rejected => Means the user should have fixed the problem already
            var sIcon = null;
            switch (this.formatter.getCurrentStatus(oEndDate, sStatusCode)) {
                case 3:
                    sIcon = "sap-icon://message-error";
                    break;
                case 2:
                    sIcon = "sap-icon://message-warning";
                    break;
                case 4:
                    sIcon = "sap-icon://decline";
                    break;
                case 1:
                    sIcon = "sap-icon://future";
                    break;
            }
            return sIcon;
        },
        statusTooltip: function (oEndDate, sStatusCode) {
            var sTooltip = null;
            switch (this.formatter.getCurrentStatus(oEndDate, sStatusCode)) {
                case 3:
                    sTooltip = this.getResourceBundle().getText("errorIconTooltip");
                    break;
                case 2:
                    sTooltip = this.getResourceBundle().getText("warningIconTooltip");
                    break;
                case 4:
                    sTooltip = this.getResourceBundle().getText("rejectIconTooltip");
                    break;
                case 1:
                    sTooltip = this.getResourceBundle().getText("successIconTooltip");
                    break;
            }
            return sTooltip;
        },
        statusColor: function (oEndDate, sStatusCode) {
            var sColor = null;
            switch (this.formatter.getCurrentStatus(oEndDate, sStatusCode)) {
                case 3:
                    sColor = "Negative";
                    break;
                case 2:
                    sColor = "Critical";
                    break;
                case 4:
                    sColor = "Default";
                    break;
                case 1:
                    sColor = "Positive";
                    break;
            }
            return sColor;
        },
        getCurrentStatus: function (oEndDate, sStatusCode) {
            // We show the success icon if the EndDate is after today AND the status is NOT completed, Canceled or rejected => means the user has still time to fix this problem
            // => This will return a 0 (=> success)
            // We show the warning icon if the EndDate is today AND the status is NOT completed, Canceled or rejected => means the user need to fix the problem today
            // => This will return a 1 (=> warning)
            // We show the error icon if the EndDate is before today AND the status is NOT completed, Canceled or rejected => Means the user should have fixed the problem already
            // => This will return a 2 (=> error)
            var iStatus = 0;
            var aStatusCodes = ["CANC", "COMP", "REJE"];
            if (sStatusCode === "REJE") {
                iStatus = 4;
            } else {
                if (
                    moment().endOf("day").isAfter(moment(oEndDate).endOf("day")) &&
                    !aStatusCodes.includes(sStatusCode)
                ) {
                    iStatus = 3;
                } else {
                    if (
                        moment().endOf("day").isBefore(moment(oEndDate).endOf("day")) &&
                        !aStatusCodes.includes(sStatusCode)
                    ) {
                        iStatus = 1;
                    } else {
                        if (
                            moment().endOf("day").isSame(moment(oEndDate).endOf("day")) &&
                            !aStatusCodes.includes(sStatusCode)
                        ) {
                            iStatus = 2;
                        }
                    }
                }
            }
            return iStatus;
        },
        formatNameWithIdInBrackets: function (sName, sNr) {
            return sNr !== "" ? sName + " (" + sNr + ")" : "";
        },
        formatAedat: function (sAedat, sToday) {
            if (sAedat === null || sAedat === "")
                return sToday;
            else
                return this.formatter.formatDateForView(sAedat);
        }
    };
});