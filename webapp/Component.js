sap.ui.define([
        "sap/ui/core/UIComponent",
        "sap/ui/Device",
        "be/amista/createpo/model/models"
    ],
    function (UIComponent, Device, models) {
        "use strict";

        return UIComponent.extend("be.amista.createpo.Component", {
            metadata: {
                manifest: "json"
            },

            /**
             * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
             * @public
             * @override
             */
            init: function () {
                // call the base component's init function
                UIComponent.prototype.init.apply(this, arguments);

                // set locale
                // sap.ui.getCore().getConfiguration().setLanguage("en-US");

                // enable routing
                this.getRouter().initialize();

                // set the device model
                this.setModel(models.createDeviceModel(), "device");

                this.setModel(models.createLocalModel(), "local");
                var sUri = this.getManifestEntry("/sap.app/dataSources/mainService/uri");
                this.getModel("local").setProperty("/Uri", sUri);
            }
        });
    }
);