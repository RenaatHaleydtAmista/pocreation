sap.ui.define(["./AjaxHelper", "sap/ui/core/Fragment",],
    function (AjaxHelper, Fragment) {
        "use strict";
        var oController,
            oUploadCollection,
            oBusinessDocumentCollection;
        return {
            initializeAttachments: function (oObjectController) {
                oController = oObjectController;
                var that = this;

                oUploadCollection = oController.byId(sap.ui.core.Fragment.createId("attachments", "uploadCollection"));
                oBusinessDocumentCollection = oController.byId(sap.ui.core.Fragment.createId("attachments", "businessDocumentCollection"));;

                this.refreshAttachmentList();
                this.refreshBusinessDocumentList();
                oController.getOwnerComponent().getModel().attachRequestCompleted(function (oEvent) {
                    that.onRequestCompleted(oEvent);
                });
            },
            /**
             * Refresh the attachment list
             */
            refreshAttachmentList: function () {
                if (oUploadCollection.getBinding("items")) {
                    oUploadCollection.getBinding("items").refresh();
                }
                this.setAttachmentsTitle();
            },
            /**
        * Refresh the attachment list
        */
            refreshBusinessDocumentList: function () {
                if (oBusinessDocumentCollection.getBinding("items")) {
                    oBusinessDocumentCollection.getBinding("items").refresh();
                }
                this.setBusinessDocumentsTitle();
            },
            /**
             * Update the title of an attachment list
             */
            setAttachmentsTitle: function () {
                var sTitle = oController.getResourceBundle().getText("attachmentsTitle");
                var aItems = oUploadCollection.getItems();
                var iNumber = aItems.length;
                var sFullTitle = sTitle + " (" + iNumber + ")";
                oController.getOwnerComponent().getModel("local").setProperty("/attachmentsTitle", sFullTitle);
                oUploadCollection.setNumberOfAttachmentsText(sFullTitle);
            },
            /**
            * Update the title of an attachment list
            */
            setBusinessDocumentsTitle: function () {
                var sTitle = oController.getResourceBundle().getText("businessDocumentsTitle");
                var aItems = oBusinessDocumentCollection.getItems();
                var iNumber = aItems.length;
                var sFullTitle = sTitle + " (" + iNumber + ")";
                oController.getOwnerComponent().getModel("local").setProperty("/businessDocumentsTitle", sFullTitle);
                oBusinessDocumentCollection.setNumberOfAttachmentsText(sFullTitle);
            },
            /**
             * Change action on file upload
             */
            onChangeAttachment: function (oEvent) {
                var csrfToken = oController.getOwnerComponent().getModel().oHeaders["x-csrf-token"];
                var oCustomerHeaderParam = new sap.m.UploadCollectionParameter({
                    name: "x-csrf-token",
                    value: csrfToken
                });
                oEvent.getSource().addHeaderParameter(oCustomerHeaderParam);
                var sUri = oEvent.getSource().getModel("local").getProperty("/Uri");
                // expand with PO header to receive GUID in CREATE_STREAM
                var sGuid = oController.getOwnerComponent().getModel().getProperty(oController.getView().getBindingContext().sPath + "/Guid");
                // var sPath = oController.getOwnerComponent().getModel().createKey("HeaderSet", {
                //     Guid: oController.getOwnerComponent().getModel().getProperty(oController.getView().getBindingContext().sPath + "/Guid")
                // });
                var sPath = oController.getHeaderSetPath(sGuid);
                var sWritePath = sUri + sPath + "/to_Files";
                oEvent.getSource().setUploadUrl(sWritePath);
            },
            /**
 * Change action on file upload
 */
            onChangeBusinessDocument: function (oEvent) {
                var csrfToken = oController.getOwnerComponent().getModel().oHeaders["x-csrf-token"];
                var oCustomerHeaderParam = new sap.m.UploadCollectionParameter({
                    name: "x-csrf-token",
                    value: csrfToken
                });
                oEvent.getSource().addHeaderParameter(oCustomerHeaderParam);
                var sUri = oEvent.getSource().getModel("local").getProperty("/Uri");
                // expand with PO header to receive GUID in CREATE_STREAM
                var sGuid = oController.getOwnerComponent().getModel().getProperty(oController.getView().getBindingContext().sPath + "/Guid");
                // var sPath = oController.getOwnerComponent().getModel().createKey("HeaderSet", {
                //     Guid: oController.getOwnerComponent().getModel().getProperty(oController.getView().getBindingContext().sPath + "/Guid")
                // });
                var sPath = oController.getHeaderSetPath(sGuid);
                var sWritePath = sUri + sPath + "/to_BusinessDocumentFiles";
                oEvent.getSource().setUploadUrl(sWritePath);
            },
            /**
             * Prepare file for uploading
             */
            onBeforeUpload: function (oEvent) {
                var name = oEvent.getParameter("fileName");
                var oCustomerHeaderSlug = new sap.m.UploadCollectionParameter({
                    name: "slug",
                    value: name
                });
                oEvent.getParameters().addHeaderParameter(oCustomerHeaderSlug);
            },
            /**
             * Upload a file
             */
            handleFileUpload: function (oEvent) {
                var status = oEvent.getParameter("files")[0].status;
                if (status === 201) {
                    // Refresh the binding for the upload collection
                    oEvent.getSource().getBinding("items").refresh()
                }
            },
            /**
             * Download a file
             */
            handleFileDownload: function (oEvent) {
                var oItem = oEvent.getSource().getParent();
                var sCounterId = oItem.getDocumentId();

                var sObjectPath = oController.getOwnerComponent().getModel().createKey("AttachmentFileSet", {
                    Guid: oController.getOwnerComponent().getModel().getProperty(oController.getView().getBindingContext().sPath + "/Guid"),
                    AttachmentId: sCounterId
                });

                var uri = oController.getVar("Uri");
                var sReadPath = uri + sObjectPath + "/$value";

                if (oItem) {
                    oItem.setUrl(sReadPath);
                    oItem.download(true);
                    oItem.setUrl(null);
                }
            },
            handleTypeMissmatch: function (oEvent) {
                sap.m.MessageBox.warning(oController.getResourceBundle().getText("typeMissmatchOfAttachment", oController.getOwnerComponent().getModel(
                    "local").getProperty("/fileTypes").toString()));
            },
            /**
             * Delete a file
             */
            handleAttachmentDelete: function (oEvent) {
                var oItem = oEvent.getParameter("item");
                var oContext = oItem.getBindingContext();
                AjaxHelper.removeObject(oController.getOwnerComponent().getModel(), oContext.sPath).catch(function (oError) {
                    oController.showErrorMessage(oError);
                });
                AjaxHelper.saveChanges(oController.getOwnerComponent().getModel());
            },
            /**
             * Handle the renaming of a file
             */
            handlefileRenamed: function (oEvent) {
                AjaxHelper.saveChanges(oController.getOwnerComponent().getModel());
            },
            /**
             * Handle request completed events. We are only interested in data for the uploadcollections!
             */
            onRequestCompleted: function (oEvent) {
                var that = this;
                var sUrl = oEvent.getParameter("url");
                if (sUrl.startsWith("AttachmentListSet")) {
                    // At the time of request completed, the data is not yet added to the upload collection object, so a delay is needed.
                    window.setTimeout(function () {
                        that.setAttachmentsTitle();
                    }, 200);
                }
                if (sUrl.startsWith("BusinessDocumentListSet")) {
                    // At the time of request completed, the data is not yet added to the upload collection object, so a delay is needed.
                    window.setTimeout(function () {
                        that.setBusinessDocumentsTitle();
                    }, 200);
                }
            },
            /**
             * Preview a PDF or image attachment
             */
            onPreview: function (oAttachment) {
                var sGuid = oController.getOwnerComponent().getModel().getProperty(oController.getView().getBindingContext().sPath + "/Guid");
                var sObjectPath = oController.getOwnerComponent().getModel().createKey("AttachmentPreviewSet", {
                    Guid: sGuid,
                    AttachmentId: oAttachment.getProperty("documentId")
                });
                var uri = oController.getVar("Uri");
                var sReadPath = uri + sObjectPath + "/$value";
                var sTitle = oAttachment.getProperty("fileName");
                window.open(sReadPath, sTitle, "resizable=yes, scrollbars=yes, titlebar=yes, width=1024, height=900, top=10, left=10");
                // if (oAttachment.getProperty("mimeType").includes("pdf")) {
                //     this.onPreviewPDF(sReadPath, sTitle);
                // }
                // if (oAttachment.getProperty("mimeType").includes("image") || oAttachment.getProperty("fileName").endsWith(".png")) {
                //     this.onPreviewPicture(sReadPath, sTitle);
                // }
            },
            /**
             * Preview a PDF attachment
             */
            onPreviewPDF: function (sPath, sTitle) {
                var that = this;
                if (!that._pdfViewer) {
                    that._pdfViewer = new sap.m.PDFViewer({
                        showDownloadButton: false,
                        displayType: "Embedded"
                    });
                    oController.getView().addDependent(this._pdfViewer);
                    that._pdfViewer.attachError(function () {
                        that._pdfViewer.destroyPopupButtons();
                    });
                }
                that._pdfViewer.setSource(sPath);
                that._pdfViewer.setTitle(sTitle);
                that._pdfViewer.setShowDownloadButton(false);

                that._pdfViewer.open();
            },
            /**
             * Preview an image attachment
             */
            onPreviewPicture: function (sPath, sTitle) {
                var that = this;
                if (!that._lightBox) {
                    that._lightBox = new sap.m.LightBox();
                    oController.getView().addDependent(that._lightBox);
                }
                that._lightBox.removeAllImageContent();
                that._lightBox.addImageContent(new sap.m.LightBoxItem({
                    imageSrc: sPath,
                    title: sTitle
                }));
                that._lightBox.open();
            },
            /**
             * Preview the PO output
             */
            onPOPreview: function (oEvent) {
                if (!oController._oDialogPreview) {
                    oController._oDialogPreview = sap.ui.xmlfragment(oController.fragmentIdPreview, "be.amista.createpo.fragments.preview", oController);
                    oController.getView().addDependent(oController._oDialogPreview);
                }
                this.setVendorLanguage();
                oController._oDialogPreview.open();
            },
            /**
             * Set the vendor's language as default preview language
             */
            setVendorLanguage: function () {
                var sVendor = oController.getView().getBindingContext().getObject().VendorNr;
                var sPath = oController.getOwnerComponent().getModel().createKey("/VL_VendorSet", {
                    VendorNr: sVendor
                });
                AjaxHelper.readEntity(oController.getOwnerComponent().getModel(), sPath).then(function (oData) {
                    oController.storeVar("VendorLanguage", oData.Language);
                }).catch(function (oError) {

                });
            },
            /**
             * Preview the PO output
             */
            previewPO: function (oEvent) {
                var sURI = oController.getVar("Uri");
                var sGuid = oController.getOwnerComponent().getModel().getProperty(oController.getView().getBindingContext().sPath + "/Guid");
                var oLanguageSelect = Fragment.byId(oController.fragmentIdPreview, "languageSelect");
                var sSpras = oLanguageSelect.getSelectedKey();
                var sPath = oController.getOwnerComponent().getModel().createKey("/PreviewSet", {
                    Guid: sGuid,
                    Spras: sSpras
                });
                var sPath = sURI + sPath + "/$value";
                // TODO: Check the status of the PO to decide which preview we should use
                // When the PO is in status "Draft", the Preview CANNOT be downloaded (by making use of the PDF viewer of the browser itself).
                // We add the #toolbar = 0 to the data uri to hide the toolbar where the download and print buttons are.
                if (oController.getView().getBindingContext().getObject().Approved) {
                    this.onPreviewPDF(sPath, "PO Preview");
                }
                // When the PO is in status "Approved", the Preview can be downloaded (by making use of the PDF Viewer)
                else {
                    window.open(sPath + "#toolbar=0", "PO Preview", "resizable=yes, scrollbars=yes, titlebar=yes, width=1024, height=900, top=10, left=10");
                }
            },

        };
    });