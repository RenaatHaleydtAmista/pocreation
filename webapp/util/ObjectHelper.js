// sap.ui.define(["sap/ui/core/Fragment"], function (Fragment) {
sap.ui.define([], function () {
    "use strict";
    return {
        getMasterViewData: function () {
            return {
                editMode: false,
                lineItems: [this.getEmptyLineItem()],
                itemSelected: true,
                selectedLineItem: {},
                acctCat: "",
                currentDate: "04/02/2022",
                today: moment().format("DD/MM/YYYY"),
                selectedVendor: {
                    Name: "",
                    Street: "",
                    PostCode: "",
                    City: "",
                    Country: "",
                    Taxnum: "",
                    Language: "",
                    Search1: "",
                    Search2: "",
                    VendorNr: "",
                },
                selectedCompanyCode: {
                    Bukrs: "",
                    Butxt: "",
                },
                selectedTaxCode: {
                    Mwskz: "",
                },
                selectedWbs: {
                    Posid: "",
                },
                selectedMaterial: {
                    Matkl: "",
                },
                selectedInternalOrder: {
                    Aufnr: "",
                },
                selectedCostCenter: {
                    Kostl: "",
                },
                selectedPurchaseGroup: {
                    Ekgrp: "",
                },
                selectedAcct: {
                    Knttp: "",
                },
                VL_VendorSet: [],
                VL_PurchasingGroupSet: [],
                VL_CompanyCodeSet: [],
                VL_AccountAssignmentSet: [],
                VL_MaterialGroupSet: [],
                VL_TaxCodeSet: []
            };
        },
        getEmptyLineItem: function () {
            return {
                editable: true,
                createMode: true,
                itemNumber: 0,
                shortText: "",
                quantity: "0",
                netPrice: "0,00",
                uom: "",
                totalPrice: "0,00",
                currency: "EUR",
                lineItemDetails: {
                    materialGroup: "",
                    accountAssignmentCategory: "",
                    deliveryDate: "04/02/2022",
                    goodsReceipt: true,
                    deliveryCompleted: false,
                    invoiceReceipt: true,
                    finalInvoice: false,
                    poText: "",
                    taxCode: "",
                    internalOrder: "",
                    internalText: "",
                },
            };
        },
    };
});