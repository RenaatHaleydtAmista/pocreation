// sap.ui.define(["sap/m/MessageBox"], function (MessageBox) {
sap.ui.define([], function () {
    "use strict";
    return {
        fetchData: function (sUrl, oController) {
            return new Promise(function (resolve, reject) {
                var url = oController
                    .getOwnerComponent()
                    .getManifestObject()
                    .resolveUri("/" + sUrl);
                var oModel = oController.getOwnerComponent().getModel();
                oModel.setDefaultBindingMode("TwoWay");
                oModel.read(url, {
                    success: function (data, response) {
                        resolve(data);
                    }.bind(this),
                    error: function (e) {
                        reject(error);
                    },
                });
            });
        },
        saveChanges: function (oModel) {
            return new Promise((resolve, reject) => {
                if (oModel.hasPendingChanges()) {
                    oModel.submitChanges({
                        success: (oData, oResponse) => {
                            oModel.resetChanges();
                            resolve(oResponse);
                        },
                        error: (oError) => reject(oError),
                    });
                } else {
                    resolve();
                }
            });
        },
        createItem: function (oModel) {
            //   return new Promise((resolve, reject) => {
            //       oModel.create()
            //   });
        },
        readEntity: function (oModel, sPath, aFilters, aSorters, oParams) {
            // Example of aSorters: [new sap.ui.model.Sorter("BillingDate", true)]
            // Example of oParameters: { $select: "SalesOrg,SalesOrgName" }
            return new Promise(function (resolve, reject) {
                oModel.read(sPath, {
                    filters: aFilters,
                    sorters: aSorters,
                    urlParameters: oParams,
                    success: function (oData) {
                        resolve(oData);
                    },
                    error: function (oResult) {
                        reject(oResult);
                    }
                });
            });
        },
        postEntry: function (oModel, sEntity, oEntry, oParams) {
            // Example of oParameters: { $select: "SalesOrg,SalesOrgName" }
            return new Promise(function (resolve, reject) {
                oModel.create(sEntity, oEntry, {
                    urlParameters: oParams,
                    success: function (oData, oResponse) {
                        resolve(oData);
                    },
                    error: function (oError) {
                        reject(oError);
                    }
                });
            });
        },
        removeObject: function (oModel, sPath) {
            return new Promise(function (resolve, reject) {
                oModel.remove(sPath, {
                    success: function (oData, oResponse) {
                        resolve(oData);
                    },
                    error: function (oError) {
                        reject(oError);
                    }
                });
            });
        }
    };
});