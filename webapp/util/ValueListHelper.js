sap.ui.define(["./AjaxHelper", "sap/ui/core/Fragment", ],
    function (AjaxHelper, Fragment) {
        "use strict";
        var oController;
        return {
            initializeValueLists: function (oObjectController) {
                oController = oObjectController;
            },
            fieldHelpRequest: function (sTextId) {
                if (!oController._oDialogFieldHelp) {
                    oController._oDialogFieldHelp = sap.ui.xmlfragment(oController.fragmentIdFieldHelp, "be.amista.createpo.fragments.fieldHelp", oController);
                    oController.getView().addDependent(oController._oDialogFieldHelp);
                }
                var sPath = oController.getOwnerComponent().getModel().createKey("/TextSet", {
                    Name: sTextId
                });
                var oText;
                oText = oController.getOwnerComponent().getModel().getProperty(sPath);
                if (!oText || typeof oText === "undefined") {
                    AjaxHelper.readEntity(oController.getOwnerComponent().getModel(), sPath, null, null, null).then(function (oData) {
                        oController.getView().getModel("masterView").setProperty("/fieldHelpText", oData.Value);
                    }).catch(function (oError) {});
                } else {
                    oController.getView().getModel("masterView").setProperty("/fieldHelpText", oText.Value);
                }
                oController._oDialogFieldHelp.open();
            },
            closeFieldHelp: function () {
                oController._oDialogFieldHelp.close();
            },
            valueHelpRequestVendor: function () {
                if (!oController._oDialogV) {
                    oController._oDialogV = sap.ui.xmlfragment(oController.fragmentIdV, "be.amista.createpo.fragments.vendors", oController);
                    oController.getView().addDependent(oController._oDialogV);
                }
                oController._oDialogV.open();
            },
            valueHelpConfirmVendor: function (oEvent) {
                var oVendor;
                if (oEvent.sId === "suggestionItemSelected") {
                    oVendor = oEvent.getParameter("selectedRow").getBindingContext().getObject();
                } else {
                    oVendor = oEvent.getParameter("selectedItem").getBindingContext().getObject();
                }
                oController.onChangeHeader("VendorNr", oVendor.VendorNr).then(function (oData) {
                    if (oData && oData.VendorNr) oController.setVendorInputValue(oData);
                });
            },
            valueHelpRequestPurGrp: function () {
                if (!oController._oDialogPurGrp) {
                    oController._oDialogPurGrp = sap.ui.xmlfragment(oController.fragmentIdPurGrp, "be.amista.createpo.fragments.purchaseGroups", oController);
                    oController.getView().addDependent(oController._oDialogPurGrp);
                }
                oController._oDialogPurGrp.open();
            },
            valueHelpConfirmPurGrp: function (oEvent) {
                var oPurGrp;
                if (oEvent.sId === "suggestionItemSelected") {
                    oPurGrp = oEvent.getParameter("selectedRow").getBindingContext().getObject();
                } else {
                    oPurGrp = oEvent.getParameter("selectedItem").getBindingContext().getObject();
                }
                oController.onChangeHeader("Ekgrp", oPurGrp.PurchasingGroup).then(function (oData) {
                    if (oData && oData.Ekgrp) oController.setEkgrpInputValue(oData.Ekgrp);
                });
            },
            valueHelpRequestCC: function () {
                if (!oController._oDialogCC) {
                    oController._oDialogCC = sap.ui.xmlfragment(oController.fragmentIdCC, "be.amista.createpo.fragments.companyCodes", oController);
                    oController.getView().addDependent(oController._oDialogCC);
                }
                oController._oDialogCC.open();
            },
            valueHelpConfirmCC: function (oEvent) {
                var oCompanyCode;
                if (oEvent.sId === "suggestionItemSelected") {
                    oCompanyCode = oEvent.getParameter("selectedRow").getBindingContext().getObject();
                } else {
                    oCompanyCode = oEvent.getParameter("selectedItem").getBindingContext().getObject();
                }
                oController.onChangeHeader("Bukrs", oCompanyCode.CompanyCode).then(function (oData) {
                    if (oData && oData.Bukrs) oController.setBukrsInputValue(oData.Bukrs);
                });
            },
            valueHelpRequestAcct: function () {
                if (!oController._oDialogAcct) {
                    oController._oDialogAcct = sap.ui.xmlfragment(oController.fragmentIdAcct, "be.amista.createpo.fragments.acctAssignment", oController);
                    oController.getView().addDependent(oController._oDialogAcct);
                }
                oController._oDialogAcct.open();
            },
            valueHelpConfirmAcct: function (oEvent) {
                if (oEvent.sId === "suggestionItemSelected") {
                    var oAcct = oEvent.getParameter("selectedRow").getBindingContext().getObject();
                } else {
                    var oAcct = oEvent.getParameter("selectedItem").getBindingContext().getObject();
                }
                var oForm = Fragment.byId(oController.getView().createId("lineItems"), "itemForm");
                var sPath = oForm.getBindingContext().getPath();
                oController.getOwnerComponent().getModel().setProperty(sPath + "/Knttp", oAcct.AccountAssignmentCategory);
                oController.getView().getModel("masterView").refresh();
                AjaxHelper.saveChanges(oController.getOwnerComponent().getModel());
            },
            valueHelpRequestInternalOrder: function () {
                if (!oController._oDialogInternalOrder) {
                    oController._oDialogInternalOrder = sap.ui.xmlfragment(oController.fragmentIdIO, "be.amista.createpo.fragments.internalOrders", oController);
                    oController.getView().addDependent(oController._oDialogInternalOrder);
                }
                if (oController.getOwnerComponent().getModel("local").getProperty("/refreshInternalOrder")) {
                    oController._oDialogInternalOrder.getBinding("items").refresh();
                    oController.getOwnerComponent().getModel("local").setProperty("/refreshInternalOrder", false);
                }
                oController._oDialogInternalOrder.open();
            },
            valueHelpConfirmInternalOrder: function (oEvent) {
                if (oEvent.sId === "suggestionItemSelected") {
                    var oInternalOrder = oEvent.getBindingContext().getObject();
                } else {
                    var oInternalOrder = oEvent.getParameter("selectedItem").getBindingContext().getObject();
                }
                var oForm = Fragment.byId(oController.getView().createId("lineItems"), "itemForm");
                var sPath = oForm.getBindingContext().getPath();
                oController.getOwnerComponent().getModel().setProperty(sPath + "/Aufnr", oInternalOrder.Aufnr);
                oController.getView().getModel("masterView").refresh();
                AjaxHelper.saveChanges(oController.getOwnerComponent().getModel());
            },
            valueHelpRequestCostCenter: function () {
                if (!oController._oDialogCostCenter) {
                    oController._oDialogCostCenter = sap.ui.xmlfragment(oController.fragmentIdCostCenter, "be.amista.createpo.fragments.costCenters", oController);
                    oController.getView().addDependent(oController._oDialogCostCenter);
                }
                if (oController.getOwnerComponent().getModel("local").getProperty("/refreshCostCenter")) {
                    oController._oDialogCostCenter.getBinding("items").refresh();
                    oController.getOwnerComponent().getModel("local").setProperty("/refreshCostCenter", false);
                }
                oController._oDialogCostCenter.open();
            },
            valueHelpConfirmCostCenter: function (oEvent) {
                if (oEvent.sId === "suggestionItemSelected") {
                    var oCostCenter = oEvent.getParameter("selectedRow").getBindingContext().getObject();
                } else {
                    var oCostCenter = oEvent.getParameter("selectedItem").getBindingContext().getObject();
                }
                var oForm = Fragment.byId(oController.getView().createId("lineItems"), "itemForm");
                var sPath = oForm.getBindingContext().getPath();
                oController.getOwnerComponent().getModel().setProperty(sPath + "/Kostl", oCostCenter.CostCenter);
                oController.getView().getModel("masterView").refresh();
                AjaxHelper.saveChanges(oController.getOwnerComponent().getModel());
            },
            valueHelpRequestWBS: function () {
                if (!oController._oDialogWBS) {
                    oController._oDialogWBS = sap.ui.xmlfragment(oController.fragmentIdWbs, "be.amista.createpo.fragments.wbs", oController);
                    oController.getView().addDependent(oController._oDialogWBS);
                }
                if (oController.getOwnerComponent().getModel("local").getProperty("/refreshWBS")) {
                    Fragment.byId(oController.fragmentIdWbs, "projectList").getBinding("items").refresh();
                    Fragment.byId(oController.fragmentIdWbs, "wbsTable").getBinding("items").refresh();
                    oController.getOwnerComponent().getModel("local").setProperty("/refreshWBS", false);
                }
                oController._oDialogWBS.open();
            },
            valueHelpRequestMatGrp: function () {
                if (!oController._oDialogMaterial) {
                    oController._oDialogMaterial = sap.ui.xmlfragment(oController.fragmentIdMG, "be.amista.createpo.fragments.materialGroups", oController);
                    oController.getView().addDependent(oController._oDialogMaterial);
                }
                oController._oDialogMaterial.open();
            },
            valueHelpConfirmMatGrp: function (oEvent) {
                if (oEvent.sId === "suggestionItemSelected") {
                    var oMaterialGroup = oEvent.getParameter("selectedRow").getBindingContext().getObject();
                } else {
                    var oMaterialGroup = oEvent.getParameter("selectedItem").getBindingContext().getObject();
                }
                var oForm = Fragment.byId(oController.getView().createId("lineItems"), "itemForm");
                var sPath = oForm.getBindingContext().getPath();
                oController.getOwnerComponent().getModel().setProperty(sPath + "/Matkl", oMaterialGroup.MaterialGroup);
                oController.getView().getModel("masterView").refresh();
                AjaxHelper.saveChanges(oController.getOwnerComponent().getModel());
            },
            valueHelpRequestAnln1: function () {
                if (!oController._oDialogAnln1) {
                    oController._oDialogAnln1 = sap.ui.xmlfragment("idDialogAsset", "be.amista.createpo.fragments.anln1", oController);
                    oController.getView().addDependent(oController._oDialogAnln1);
                }
                if (oController.getOwnerComponent().getModel("local").getProperty("/refreshAsset")) {
                    oController._oDialogAnln1.getBinding("items").refresh();
                    oController.getOwnerComponent().getModel("local").setProperty("/refreshAsset", false);
                }
                oController._oDialogAnln1.open();
            },
            valueHelpConfirmAnln1: function (oEvent) {
                if (oEvent.sId === "suggestionItemSelected") {
                    var oAsset = oEvent.getParameter("selectedRow").getBindingContext().getObject();
                } else {
                    var oAsset = oEvent.getParameter("selectedItem").getBindingContext().getObject();
                }
                var oForm = Fragment.byId(oController.getView().createId("lineItems"), "itemForm");
                var sPath = oForm.getBindingContext().getPath();
                oController.getOwnerComponent().getModel().setProperty(sPath + "/Anln1", oAsset.Anln1);
                oController.getView().getModel("masterView").refresh();
                AjaxHelper.saveChanges(oController.getOwnerComponent().getModel());
            },
            valueHelpRequestTaxCode: function () {
                if (!oController._oDialogTaxCode) {
                    oController._oDialogTaxCode = sap.ui.xmlfragment(oController.fragmentIdTaxCode, "be.amista.createpo.fragments.taxCodes", oController);
                    oController.getView().addDependent(oController._oDialogTaxCode);
                }
                if (oController.getOwnerComponent().getModel("local").getProperty("/refreshTaxCode")) {
                    oController._oDialogTaxCode.getBinding("items").refresh();
                    oController.getOwnerComponent().getModel("local").setProperty("/refreshTaxCode", false);
                }
                oController._oDialogTaxCode.open();
            },
            valueHelpConfirmTaxCode: function (oEvent) {
                if (oEvent.sId === "suggestionItemSelected") {
                    var oTaxCode = oEvent.getParameter("selectedRow").getBindingContext().getObject();
                } else {
                    var oTaxCode = oEvent.getParameter("selectedItem").getBindingContext().getObject();
                }
                var oForm = Fragment.byId(oController.getView().createId("lineItems"), "itemForm");
                var sPath = oForm.getBindingContext().getPath();
                oController.getOwnerComponent().getModel().setProperty(sPath + "/Mwskz", oTaxCode.Mwskz);
                oController.getView().getModel("masterView").refresh();
                AjaxHelper.saveChanges(oController.getOwnerComponent().getModel());
            },
        };
    });